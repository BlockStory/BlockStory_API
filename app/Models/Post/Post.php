<?php

namespace App\Models\Post;

use App\Modules\Commentable;
use App\Modules\IdUUID;
use App\Modules\Likeable;
use App\Modules\Taggable;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use IdUUID, Likeable, Commentable, Taggable;

    public $incrementing = false;

    protected $guarded = [];

    protected $with = ['likes'];

    protected $casts = [
        'allow_comments' => 'boolean',
        'published' => 'boolean',
        'sandbox' => 'boolean'
    ];

    protected $dates = [
        'posted',
        'updated',
        'created'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
