<?php

namespace App\Models\Comment;

use App\Models\Game\Game;
use App\Models\Post\Post;
use App\Modules\IdUUID;
use App\Modules\Likeable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use IdUUID, Likeable, SoftDeletes;

    const MODEL_TYPES = [
        'post' => Post::class,
        'game' => Game::class
    ];

    public $incrementing = false;

    protected $guarded = [];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
