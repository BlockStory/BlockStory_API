<?php

namespace App\Models\Review;

use App\Modules\Commentable;
use App\Modules\IdUUID;
use App\Modules\Likeable;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use IdUUID, Commentable, Likeable;

    public $incrementing = false;

    protected $guarded = [];

    protected $casts = [
        'allow_comments' => 'boolean',
        'published' => 'boolean'
    ];

    protected $dates = [
        'crated_at',
        'updated_at',
        'published_at'
    ];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
