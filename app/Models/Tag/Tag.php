<?php

namespace App\Models\Tag;

use App\Models\Post\Post;
use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $guarded = [];

    public function taggable()
    {
        return $this->morphTo();
    }

    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
    }

    public function incrementUsage()
    {
        $this->increment('usage');

        return $this;
    }
}
