<?php

namespace App\Models\File;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use IdUUID;

    const TYPE = File::TYPE_IMAGE;

    public $incrementing = false;

    protected $guarded = [];

    public function file()
    {
        return $this->morphOne(File::class, 'fileable');
    }
}
