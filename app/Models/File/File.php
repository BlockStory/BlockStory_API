<?php

namespace App\Models\File;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    use IdUUID;

    const DISK = 'public';
    const TYPE_IMAGE = 'image';

    public $incrementing = false;

    protected $guarded = [];

    public function fileable()
    {
        return $this->morphTo();
    }

    /**
     * @param UploadedFile $file
     * @param string $id
     * @param string $class
     * @param string $type
     * @return $this|Model
     */
    public static function uploaded(UploadedFile $file, string $id, string $class, string $type)
    {
        return static::create([
            'user_id' => \Auth::user()->id,
            'fileable_id' => $id,
            'fileable_type' => $class,
            'type' => $type,
            'size' => $file->getSize(),
            'name_original' => $file->getClientOriginalName(),
            'extension' => $file->extension(),
            'extension_original' => $file->getClientOriginalExtension(),
            'path' => Storage::disk(self::DISK)->putFile($type . '/' . \Auth::user()->id, $file)
        ]);
    }
}
