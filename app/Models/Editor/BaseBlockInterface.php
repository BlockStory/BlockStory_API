<?php

namespace App\Models\Editor;

interface BaseBlockInterface
{
    public function block();
    public function updateBlock($data);
}