<?php

namespace App\Models\Editor;

use App\Models\Editor\Block\Block;
use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $guarded = [];

    public function blocks()
    {
        return $this->hasMany(Block::class);
    }

    public function starterBlock()
    {
        return $this->belongsTo(Block::class, 'starter_block_id');
    }
}
