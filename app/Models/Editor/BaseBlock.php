<?php

namespace App\Models\Editor;

use App\Exceptions\API\V1\ApiBlockUpdateException;
use App\Models\Editor\Block\Block;
use App\Models\Play\Play;
use App\Modules\IdUUID;
use App\Modules\Output;
use Illuminate\Database\Eloquent\Model;
use Validator;

abstract class BaseBlock extends Model implements BaseBlockInterface
{
    use IdUUID, Output;

    const TYPE = '';
    const RESOURCE = '';

    public $incrementing = false;
    protected $guarded = [];

    public $extraBlockUpdated = [];

    function block() {
        return $this->morphOne(Block::class, 'blockable');
    }

    abstract function updateBlock($data);

    public function beforeRender(Play &$play)
    {

    }

    public function beforeExitRender(Play &$play)
    {

    }

    public function renderBlock(Play &$play)
    {
        return null;
    }

    /**
     * @param string|null $nextBlockId
     * @param string $attributeName
     * @return bool
     * @throws \Exception
     */
    protected function setNextBlock(string $nextBlockId = null, $attributeName = 'next_block') {
        if ($nextBlockId == null) {
            $this->setAttribute($attributeName, null);
        } else {
            $nextBlock = Block::findOrFail(
                $nextBlockId
            );

            if ($nextBlock->getAttribute('game_id') != $this->getAttribute('block')->game_id) {
                throw new \Exception('This block not accepted to current game');
            }

            $this->setAttribute($attributeName, $nextBlock->getAttribute('id'));
        }

        return $this->save();
    }

    /**
     * @param $data
     * @param $rules
     * @param bool $notThrow
     * @return bool
     * @throws ApiBlockUpdateException
     */
    public function validate($data, $rules, $notThrow = false) {
        $validator = Validator::make($data, $rules);

        if (!$notThrow && $validator->fails()) {
            throw new ApiBlockUpdateException($this->setErrorValidatorEndRender($validator));
        }

        return $validator->fails();
    }
}
