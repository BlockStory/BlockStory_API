<?php

namespace App\Models\Editor\Block;

use App\Models\Editor\Chapter;
use App\Models\Game\Game;
use App\Modules\IdUUID;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $guarded = [];

    public function blockable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param string $type
     * @param Game|Model $game
     * @param Chapter|Model $chapter
     * @param array $position
     * @param array $with
     * @return null|static
     * @throws \Exception
     */
    public static function createBlock(string $type, Game $game, Chapter $chapter, array $position = [], array $with = ['blockable'])
    {
        if (!key_exists($type, config('blocks.types'))) {
            throw new \Exception('Block type not exists');
        }

        $instanceBlock = (config('blocks.types')[$type])::create() ;

        $block = self::create([
            'user_id'       => \Auth::id(),
            'game_id'       => $game->getAttribute('id'),
            'chapter_id'    => $chapter->getAttribute('id'),
            'title'         => 'New block (' . strtoupper(str_random(5)) . ')',
            'position_left' => array_get($position, 'left', 0),
            'position_top'  => array_get($position, 'top', 0),
            'blockable_type'    => get_class($instanceBlock),
            'blockable_id'      => $instanceBlock->id
        ]);

        return $block->fresh($with);
    }
}
