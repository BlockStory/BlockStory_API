<?php

namespace App\Models\Editor\Block\Param;

use App\Http\Resources\API\V1\Blocks\Param\SetParamResource;
use App\Models\Editor\BaseBlock;
use App\Models\Game\Param;
use App\Models\Play\Play;

class SetParam extends BaseBlock
{
    const TYPE = 'set_param';
    const RESOURCE = SetParamResource::class;

    protected $table = 'block_params_set';

    protected $casts = [
        'value_is_param' => 'boolean'
    ];

    public function param()
    {
        return $this->belongsTo(Param::class);
    }

    public function baseParam()
    {
        return $this->belongsTo(Param::class, 'base_param_id');
    }

    /**
     * @param Play $play
     * @return mixed|null
     * @throws \Exception
     */
    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        $param = $play
            ->params()
            ->where('param_id', '=', $this->getAttribute('param_id'))
            ->first();

        if (!$param) {
            throw new \Exception('Param not founded');
        }

        if ($this->getAttribute('value_is_param')) {
            $changeParam = $play
                ->params()
                ->where('param_id', '=', $this->getAttribute('value_param_id'))
                ->first();

            if (!$changeParam) {
                throw new \Exception('Change param not set');
            }

            $changeValue = $changeParam->getAttribute('value');

            if ($param->getAttribute('baseParam')->type == Param::TYPE_INTEGER) {
                if ($changeParam->getAttribute('baseParam')->type != Param::TYPE_INTEGER) {
                    throw new \Exception('Change param should be integer');
                }
            }

            if ($param->getAttribute('baseParam')->type == Param::TYPE_BOOLEAN) {
                if ($changeParam->getAttribute('baseParam')->type != Param::TYPE_BOOLEAN) {
                    throw new \Exception('Change param should be integer');
                }
            }
        } else {
            $changeValue = $this->getAttribute('value');
        }

        $param->setAttribute('value', $changeValue);

        $param->save();

        return $this->getAttribute('next_block');
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block_updated'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('param', $data)) {
            $this->extraBlockUpdated['param'] = $this->updateParam($data['param']);
        }

        if (key_exists('value_param', $data)) {
            $this->extraBlockUpdated['value_param'] = $this->updateParam($data['value_param'], 'value_param_id');
        }

        if (key_exists('value', $data)) {
            $this->extraBlockUpdated['value'] = $this->updateValue($data['value']);
        }

        if (key_exists('value_is_param', $data)) {
            $this->extraBlockUpdated['value_is_param'] = $this->updateValueIsParam($data['value_is_param']);
        }

        return $this->extraBlockUpdated;
    }

    /**
     * @param $paramId
     * @param string $attribute
     * @return bool
     * @throws \Exception
     */
    public function updateParam($paramId, $attribute = 'param_id')
    {
        if (!$paramId) {
            $this->setAttribute($attribute, null);
        } else {
            $param = Param::findOrFail($paramId);

            if ($param->getAttribute('game_id') !== $this->getAttribute('block')->game_id) {
                throw new \Exception('Current block not belong to game');
            }

            if ($this->getAttribute('param')->type == Param::TYPE_INTEGER) {
                if ($param->getAttribute('type') != Param::TYPE_INTEGER) {
                    throw new \Exception('Set param should be integer');
                }
            }

            if ($this->getAttribute('param')->type == Param::TYPE_BOOLEAN) {
                if ($param->getAttribute('type') != Param::TYPE_BOOLEAN) {
                    throw new \Exception('Set param should be boolean');
                }
            }

            $this->setAttribute($attribute, $param->getAttribute('id'));
        }

        return $this->save();
    }

    /**
     * @param $value
     * @return bool
     * @throws \Exception
     */
    public function updateValue($value) {
        if ($this->getAttribute('param')->type == Param::TYPE_INTEGER) {
            $this->validate(['value' => $value], [
                'value' => 'nullable|numeric'
            ]);
        }

        $this->setAttribute('value', $value);

        return $this->save();
    }

    /**
     * @param $value
     * @return bool
     * @throws \Exception
     */
    public function updateValueIsParam($value) {
        $this->validate(['value_is_param' => $value], [
            'value_is_param' => 'required|boolean'
        ]);

        $this->setAttribute('value_is_param', $value);

        return $this->save();
    }
}
