<?php

namespace App\Models\Editor\Block\Param;

use App\Http\Resources\API\V1\Blocks\Param\InputParamResource;
use App\Models\Editor\BaseBlock;
use App\Models\Game\Param;

class InputParam extends BaseBlock
{
    const TYPE = 'input_param';
    const RESOURCE = InputParamResource::class;

    protected $table = 'block_params_input';

    public function param()
    {
        return $this->belongsTo(Param::class);
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block_updated'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('param', $data)) {
            $this->updateParam($data['param']);
        }

        if (key_exists('rule', $data)) {
            $this->updateRule($data['rule']);
        }

        if (key_exists('text', $data)) {
            $this->updateText($data['text']);
        }

        $this->save();

        return $this->extraBlockUpdated;
    }

    /**
     * @param $paramId
     * @param string $attribute
     * @return $this
     * @throws \Exception
     */
    public function updateParam($paramId, $attribute = 'param_id')
    {
        if (!$paramId) {
            $this->setAttribute($attribute, null);
        } else {
            $param = Param::findOrFail($paramId);

            if ($param->getAttribute('game_id') !== $this->getAttribute('block')->game_id) {
                throw new \Exception('Current block not belong to game');
            }

            $this->setAttribute($attribute, $param->getAttribute('id'));
        }

        $this->extraBlockUpdated['param'] = $paramId;

        return $this;
    }

    public function updateRule($value)
    {
        $this->setAttribute('rule', $value);

        $this->extraBlockUpdated['rule'] = $value;

        return $this;
    }

    public function updateText($value)
    {
        $this->setAttribute('text', $value);

        $this->extraBlockUpdated['text'] = $value;

        return $this;
    }
}
