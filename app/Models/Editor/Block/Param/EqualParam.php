<?php

namespace App\Models\Editor\Block\Param;

use App\Http\Resources\API\V1\Blocks\Param\EqualParamResource;
use App\Models\Editor\BaseBlock;
use App\Models\Game\Param;
use App\Models\Play\Play;

class EqualParam extends BaseBlock
{
    const TYPE = 'equal_params';
    const RESOURCE = EqualParamResource::class;

    protected $table = 'block_params_equal';

    protected $casts = [
        'value_is_param' => 'boolean'
    ];

    /**
     * @param Play $play
     * @return mixed|null
     * @throws \Exception
     */
    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        $param = $play
            ->params()
            ->where('param_id', '=', $this->getAttribute('param_id'))
            ->first();

        if (!$param) {
            throw new \Exception('Param not founded');
        }

        if ($this->getAttribute('value_is_param')) {
            $changeParam = $play
                ->params()
                ->where('param_id', '=', $this->getAttribute('value_param_id'))
                ->without('baseParam')
                ->first();

            if (!$changeParam) {
                throw new \Exception('Change param not set');
            }

            $changeValue = $changeParam->getAttribute('value');
        } else {
            $changeValue = $this->getAttribute('value');
        }

        return $param->getAttribute('value') == $changeValue
            ? $this->getAttribute('next_block_true')
            : $this->getAttribute('next_block_false');
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('param', $data)) {
            $this->extraBlockUpdated['param'] = $this->updateParam($data['param']);
        }

        if (key_exists('value_param', $data)) {
            $this->extraBlockUpdated['value_param'] = $this->updateParam($data['value_param'], 'value_param_id');
        }

        if (key_exists('value', $data)) {
            $this->extraBlockUpdated['value'] = $this->updateValue($data['value']);
        }

        if (key_exists('value_is_param', $data)) {
            $this->extraBlockUpdated['value_is_param'] = $this->updateValueIsParam($data['value_is_param']);
        }

        if (key_exists('true_block', $data)) {
            $this->extraBlockUpdated['true_block'] = $this->setNextBlock($data['true_block'], 'next_block_true');
        }

        if (key_exists('false_block', $data)) {
            $this->extraBlockUpdated['false_block'] = $this->setNextBlock($data['false_block'], 'next_block_false');
        }

        return $this->extraBlockUpdated;
    }

    /**
     * @param $paramId
     * @param string $attribute
     * @return bool
     * @throws \Exception
     */
    public function updateParam($paramId, $attribute = 'param_id')
    {
        if (!$paramId) {
            $this->setAttribute($attribute, null);
        } else {
            $param = Param::findOrFail($paramId);

            if ($param->getAttribute('game_id') !== $this->getAttribute('block')->game_id) {
                throw new \Exception('Current block not belong to game');
            }

            $this->setAttribute($attribute, $param->getAttribute('id'));
        }

        return $this->save();
    }

    /**
     * @param $value
     * @return bool
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateValue($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'nullable|numeric'
        ]);

        $this->setAttribute('value', $value);

        return $this->save();
    }

    /**
     * @param $value
     * @return bool
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateValueIsParam($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'required|boolean'
        ]);

        $this->setAttribute('value_is_param', $value);

        return $this->save();
    }
}
