<?php

namespace App\Models\Editor\Block\Text;

use App\Http\Resources\API\V1\Blocks\TextInputResource;
use App\Models\Editor\BaseBlock;
use App\Models\Game\Param;
use App\Models\Play\Play;

class TextInput extends BaseBlock
{
    const TYPE = 'text_input';
    const RESOURCE = TextInputResource::class;

    protected $table = 'block_text_inputs';

    protected $with = ['param'];

    public function param()
    {
        return $this->belongsTo(Param::class);
    }

    /**
     * @param Play $play
     * @throws \Exception
     */
    public function beforeExitRender(Play &$play)
    {
        $baseParam = $this->getAttribute('param');

        request()->validate([
            'input' => 'required|' . $baseParam->type
        ]);

        $input = request('input');
        $param = $play->params()->where('param_id', '=', $baseParam->id)->first();

        if (!$param) {
            throw new \Exception('Param not founded');
        }

        $param->setAttribute('value', $input);
        $param->save();
    }

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('text', $data)) {
            $this->extraBlockUpdated['text_updated'] = $this->updateText($data['text']);
        }

        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block_updated'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('param', $data)) {
            $this->extraBlockUpdated['param'] = $this->updateParam($data['param']);
        }

        return $this->extraBlockUpdated;
    }

    public function updateText($text)
    {
        $this->setAttribute('text', $text);

        return $this->save();
    }

    /**
     * @param $paramId
     * @return bool
     * @throws \Exception
     */
    public function updateParam($paramId)
    {
        if (!$paramId) {
            $this->setAttribute('param_id', null);
        } else {
            $param = Param::findOrFail($paramId);

            if ($param->getAttribute('game_id') !== $this->getAttribute('block')->game_id) {
                throw new \Exception('Current block not belong to game');
            }

            $this->setAttribute('param_id', $param->getAttribute('id'));
        }

        return $this->save();
    }
}
