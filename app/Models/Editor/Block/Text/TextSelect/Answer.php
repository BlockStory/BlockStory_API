<?php

namespace App\Models\Editor\Block\Text\TextSelect;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $table = 'block_textselect_answers';

    protected $guarded = [];
}
