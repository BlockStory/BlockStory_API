<?php

namespace App\Models\Editor\Block\Text;

use App\Http\Resources\API\V1\Blocks\Text\AnswerResource;
use App\Http\Resources\API\V1\Blocks\TextSelectAnswerResource;
use App\Models\Editor\BaseBlock;
use App\Models\Editor\Block\Block;
use App\Models\Editor\Block\Text\TextSelect\Answer;

class TextSelectAnswer extends BaseBlock
{
    const TYPE = 'text_select';
    const RESOURCE = TextSelectAnswerResource::class;

    protected $table = 'block_textselects';

    protected $with = [
        'answers'
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class, 'block_id');
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('text', $data)) {
            $this->extraBlockUpdated['text_updated'] = $this->updateText($data['text']);
        }

        if (key_exists('new_answer', $data)) {
            $this->extraBlockUpdated['answer_created'] = $this->createNewAnswer($data['new_answer']);
        }

        if (key_exists('delete_answer', $data)) {
            $this->extraBlockUpdated['answer_deleted'] = $this->deleteAnswer($data['delete_answer']);
        }

        if (key_exists('update_variant', $data)) {
            $this->extraBlockUpdated['update_variant'] = $this->updateAnswer($data['update_variant']);
        }

        return $this->extraBlockUpdated;
    }

    public function updateText($text)
    {
        $this->setAttribute('text', $text);

        return $this->save();
    }

    public function createNewAnswer($title)
    {
        $answer = Answer::create([
            'block_id' => $this->getAttribute('id'),
            'title' => $title
        ]);

         return new AnswerResource($answer->fresh());
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function updateAnswer($data)
    {
        $answer = Answer::findOrFail(
            $data['variant']
        );

        if ($answer->getAttribute('block_id') !== $this->getAttribute('id')) {
            throw new \Exception('Answer not accepted to current block');
        }

        if (key_exists('next_block', $data)) {
            if ($data['next_block'] == null) {
                $answer->update([
                    'next_block' => null
                ]);
            } else {
                $nextBlock = Block::findOrFail(
                    $data['next_block']
                );

                if ($nextBlock->getAttribute('game_id') != $this->getAttribute('block')->game_id) {
                    throw new \Exception('This block not accepted to current game');
                }

                $answer->update([
                    'next_block' => $nextBlock->getAttribute('id')
                ]);
            }
        }

        return $answer->save();
    }

    /**
     * @param $answerId
     * @return bool|null
     * @throws \Exception
     */
    public function deleteAnswer($answerId)
    {
        $answer = Answer::findOrFail(
            $answerId
        );

        if ($answer->getAttribute('block_id') !== $this->getAttribute('id')) {
            throw new \Exception('Answer not accepted to current block');
        }

        return $answer->delete();
    }
}
