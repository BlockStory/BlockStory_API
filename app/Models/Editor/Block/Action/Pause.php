<?php

namespace App\Models\Editor\Block\Action;

use App\Http\Resources\API\V1\Blocks\PauseResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;
use Carbon\Carbon;

class Pause extends BaseBlock
{
    const TYPE = 'pause_block';
    const RESOURCE = PauseResource::class;

    protected $table = 'block_pauses';

    protected $casts = [
        'show_time' => 'boolean'
    ];

    public function beforeRender(Play &$play)
    {
        parent::beforeRender($play);
    }

    /**
     * @param Play $play
     * @throws \Exception
     */
    public function beforeExitRender(Play &$play)
    {
        parent::beforeExitRender($play);

        if ($play->getAttribute('wait_until') != null) {
            if (Carbon::now()->lessThan($play->getAttribute('wait_until'))) {
                throw new \Exception('Not a time');
            }
        }

        $play->setAttribute('wait_until', null);
        $play->save();
    }

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        if (!$play->getAttribute('wait_until')) {
            $timestamp = Carbon::now()
                ->addSeconds($this->getAttribute('seconds'))
                ->addMinutes($this->getAttribute('minutes'))
                ->addHours($this->getAttribute('hours'))
                ->addDays($this->getAttribute('days'));

            $play->setAttribute('wait_until', $timestamp->toDateTimeString());
        }
    }

    /**
     * @param $data
     * @return array
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('text', $data)) {
            $this->extraBlockUpdated['text'] = $this->updateText($data['text']);
        }

        if (key_exists('show_time', $data)) {
            $this->extraBlockUpdated['show_time'] = $this->updateShowTime($data['show_time']);
        }

        if (key_exists('time', $data)) {
            $this->extraBlockUpdated['time'] = $this->updateTime($data['time']);
        }

        return $this->extraBlockUpdated;
    }

    /**
     * @param $value
     * @return bool
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateShowTime($value) {
        $this->validate(['value' => $value], [
           'value' => 'required|boolean'
        ]);

        $this->setAttribute('show_time', $value);

        return $this->save();
    }

    /**
     * @param $value
     * @return bool
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateTime($value) {
        $this->validate(['value' => $value], [
            'value' => 'required|array',
            'value.days' => 'required|integer|min:0|max:7',
            'value.hours' => 'required|integer|min:0|max:23',
            'value.minutes' => 'required|integer|min:0|max:59',
            'value.seconds' => 'required|integer|min:0|max:59'
        ]);

        $this->setAttribute('days', $value['days']);
        $this->setAttribute('hours', $value['hours']);
        $this->setAttribute('minutes', $value['minutes']);
        $this->setAttribute('seconds', $value['seconds']);

        return $this->save();
    }

    public function updateText($value)
    {
        $this->setAttribute('text', $value);

        return $this->save();
    }
}
