<?php

namespace App\Models\Editor\Block\Action;

use App\Http\Resources\API\V1\Blocks\CheckpointResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;

class Checkpoint extends BaseBlock
{
    const TYPE = 'checkpoint_block';
    const RESOURCE = CheckpointResource::class;

    protected $table = 'block_checkpoints';

    protected $casts = [
        'always_available' => 'boolean'
    ];

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        return $this->getAttribute('next_block');
    }


    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('title', $data)) {
            $this->extraBlockUpdated['title'] = $this->updateTitle($data['title']);
        }

        if (key_exists('description', $data)) {
            $this->extraBlockUpdated['description'] = $this->updateDescription($data['description']);
        }

        if (key_exists('always_available', $data)) {
            $this->extraBlockUpdated['always_available'] = $this->updateAvailble($data['always_available']);
        }

        $this->save();

        return $this->extraBlockUpdated;
    }

    public function updateTitle($value) {
        $this->setAttribute('title', $value);

        return $value;
    }

    public function updateDescription($value) {
        $this->setAttribute('description', $value);

        return $value;
    }

    /**
     * @param $value
     * @return bool
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateAvailble($value) {
        $this->validate(['value' => $value], [
            'value' => 'required|boolean'
        ]);

        $this->setAttribute('always_available', $value);

        return $value;
    }
}
