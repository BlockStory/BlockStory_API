<?php

namespace App\Models\Editor\Block\Action;

use App\Http\Resources\API\V1\Blocks\NextChapterResource;
use App\Models\Editor\BaseBlock;
use App\Models\Editor\Chapter;
use App\Models\Play\Play;

class NextChapter extends BaseBlock
{
    const TYPE = 'next_chapter';
    const RESOURCE = NextChapterResource::class;

    protected $table = 'block_nextChapters';

    public function nextChapter()
    {
        return $this->belongsTo(Chapter::class, 'next_chapter');
    }

    /**
     * @param Play $play
     * @return null
     * @throws \Exception
     */
    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        $chapter = $this->getAttribute('nextChapter');

        if (!$chapter) {
            throw new \Exception('Chapter not setted');
        }

        $nextBlock = $chapter->getAttribute('starter_block_id');

        if (!$nextBlock) {
            throw new \Exception('Starter block not setted');
        }

        return $nextBlock;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_chapter', $data)) {
            $this->updateNextChapter($data['next_chapter']);
        }
    }

    /**
     * @param $nextChapterId
     * @return array
     * @throws \Exception
     */
    protected function updateNextChapter($nextChapterId)
    {
        if ($nextChapterId == null) {
            $this->setAttribute('next_chapter', null);
            $this->extraBlockUpdated['next_chapter'] = null;
        } else {
            $nextChapter = Chapter::findOrFail(
                $nextChapterId
            );

            if ($nextChapter->getAttribute('game_id') != $this->getAttribute('block')->game_id) {
                throw new \Exception('This chapter not accepted to current game');
            }

            $this->setAttribute('next_chapter', $nextChapter->getAttribute('id'));
            $this->extraBlockUpdated['next_chapter'] = null;
        }

        $this->save();
        return $this->extraBlockUpdated;
    }
}
