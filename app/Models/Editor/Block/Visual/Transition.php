<?php

namespace App\Models\Editor\Block\Visual;

use App\Http\Resources\API\V1\Blocks\TransitionResource;
use App\Models\Editor\BaseBlock;

class Transition extends BaseBlock
{
    const TYPE = 'transition_block';
    const RESOURCE = TransitionResource::class;

    protected $table = 'block_visual_transitions';


    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('enter', $data)) {
            $this->extraBlockUpdated['enter'] = $this->updateEnter($data['enter']);
        }

        if (key_exists('leave', $data)) {
            $this->extraBlockUpdated['leave'] = $this->updateLeave($data['leave']);
        }

        $this->save();

        return $this->extraBlockUpdated;
    }

    /**
     * @param $value
     * @return mixed
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateEnter($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'nullable|string|in:' . implode(',', config('blocks.data.visual.' . self::TYPE . '.transitions'))
        ]);

        $this->setAttribute('enter', $value);

        return $value;
    }

    /**
     * @param $value
     * @return mixed
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateLeave($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'nullable|string|in:' . implode(',', config('blocks.data.visual.' . self::TYPE . '.transitions'))
        ]);

        $this->setAttribute('leave', $value);

        return $value;
    }
}
