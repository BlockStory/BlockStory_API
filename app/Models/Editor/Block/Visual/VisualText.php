<?php

namespace App\Models\Editor\Block\Visual;

use App\Http\Resources\API\V1\Blocks\VisualTextResource;
use App\Models\Editor\BaseBlock;

class VisualText extends BaseBlock
{
    const TYPE = 'visual_text_block';
    const RESOURCE = VisualTextResource::class;

    protected $table = 'block_visual_texts';

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('color_light', $data)) {
            $this->extraBlockUpdated['color_light'] = $this->updateColor($data['color_light'], 'color_light');
        }

        if (key_exists('color_dark', $data)) {
            $this->extraBlockUpdated['color_dark'] = $this->updateColor($data['color_dark'], 'color_dark');
        }

        $this->save();

        return $this->extraBlockUpdated;
    }

    public function updateColor($value, $attribute = 'color_light')
    {
        $this->setAttribute($attribute, ltrim($value, '#'));

        return $value;
    }
}
