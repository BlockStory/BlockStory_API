<?php

namespace App\Models\Editor\Block\Visual;

use App\Http\Resources\API\V1\Blocks\BackgroundResource;
use App\Models\Editor\BaseBlock;

class Background extends BaseBlock
{
    const TYPE = 'background_block';
    const RESOURCE = BackgroundResource::class;

    protected $table = 'block_visual_backgrounds';

    protected $casts = [
        'body_dark_is_color_gradient' => 'boolean',
        'body_light_is_color_gradient' => 'boolean',
        'card_dark_is_color_gradient' => 'boolean',
        'card_light_is_color_gradient' => 'boolean',
    ];

    /**
     * @param $data
     * @return array
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('next_block', $data)) {
            $this->extraBlockUpdated['next_block'] = $this->setNextBlock($data['next_block']);
        }

        if (key_exists('card', $data)) {
            $this->extraBlockUpdated['card'] = $this->updateCard($data['card']);
        }

        if (key_exists('body', $data)) {
            $this->extraBlockUpdated['body'] = $this->updateBody($data['body']);
        }

        $this->save();

        return $this->extraBlockUpdated;
    }

    /**
     * @param $value
     * @return mixed
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateCard($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'required|array',
            'value.light' => 'required|array',
            'value.dark' => 'required|array',
            'value.light.is_color_gradient' => 'required|boolean',
            'value.light.first' => 'nullable',
            'value.light.second' => 'nullable',
            'value.light.color' => 'nullable',
            'value.dark.is_color_gradient' => 'required|boolean',
            'value.dark.first' => 'nullable',
            'value.dark.second' => 'nullable',
            'value.dark.color' => 'nullable',
        ]);

        $this->setAttribute('card_light_is_color_gradient', $value['light']['is_color_gradient']);
        $this->setAttribute('card_light_color_gradient_first', ltrim($value['light']['first'], '#'));
        $this->setAttribute('card_light_color_gradient_second', ltrim($value['light']['second'], '#'));
        $this->setAttribute('card_light_color', ltrim($value['light']['color'], '#'));

        $this->setAttribute('card_dark_is_color_gradient', $value['dark']['is_color_gradient']);
        $this->setAttribute('card_dark_color_gradient_first', ltrim($value['dark']['first'], '#'));
        $this->setAttribute('card_dark_color_gradient_second', ltrim($value['dark']['second'], '#'));
        $this->setAttribute('card_dark_color', ltrim($value['dark']['color'], '#'));

        return $value;
    }

    /**
     * @param $value
     * @return mixed
     * @throws \App\Exceptions\API\V1\ApiBlockUpdateException
     */
    public function updateBody($value)
    {
        $this->validate(['value' => $value], [
            'value' => 'required|array',
            'value.light' => 'required|array',
            'value.dark' => 'required|array',
            'value.light.is_color_gradient' => 'required|boolean',
            'value.light.first' => 'nullable',
            'value.light.second' => 'nullable',
            'value.light.color' => 'nullable',
            'value.dark.is_color_gradient' => 'required|boolean',
            'value.dark.first' => 'nullable',
            'value.dark.second' => 'nullable',
            'value.dark.color' => 'nullable',
        ]);

        $this->setAttribute('body_light_is_color_gradient', $value['light']['is_color_gradient']);
        $this->setAttribute('body_light_color_gradient_first', ltrim($value['light']['first'], '#'));
        $this->setAttribute('body_light_color_gradient_second', ltrim($value['light']['second'], '#'));
        $this->setAttribute('body_light_color', ltrim($value['light']['color'], '#'));

        $this->setAttribute('body_dark_is_color_gradient', $value['dark']['is_color_gradient']);
        $this->setAttribute('body_dark_color_gradient_first', ltrim($value['dark']['first'], '#'));
        $this->setAttribute('body_dark_color_gradient_second', ltrim($value['dark']['second'], '#'));
        $this->setAttribute('body_dark_color', ltrim($value['dark']['color'], '#'));

        return $value;
    }
}
