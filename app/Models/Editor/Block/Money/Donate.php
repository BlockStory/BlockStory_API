<?php

namespace App\Models\Editor\Block\Money;

use App\Http\Resources\API\V1\Blocks\DonateResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;

class Donate extends BaseBlock
{
    const TYPE = 'donate_block';
    const RESOURCE = DonateResource::class;

    protected $table = 'block_donates';

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        return $this->getAttribute('next_block');
    }

    public function updateBlock($data)
    {
        // TODO: Implement updateBlock() method.
    }
}
