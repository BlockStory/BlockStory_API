<?php

namespace App\Models\Editor\Block\Money;

use App\Http\Resources\API\V1\Blocks\AdvertisementResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;

class Advertisement extends BaseBlock
{
    const TYPE = 'advertisement_block';
    const RESOURCE = AdvertisementResource::class;

    protected $table = 'block_advertisements';

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        return $this->getAttribute('next_block');
    }

    public function updateBlock($data)
    {
        // TODO: Implement updateBlock() method.
    }
}
