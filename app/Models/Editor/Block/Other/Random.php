<?php

namespace App\Models\Editor\Block\Other;

use App\Http\Resources\API\V1\Blocks\Random\RandomVariantResource;
use App\Http\Resources\API\V1\Blocks\RandomResource;
use App\Models\Editor\BaseBlock;
use App\Models\Editor\Block\Block;
use App\Models\Editor\Block\Other\Random\Variant;
use App\Models\Play\Play;

class Random extends BaseBlock
{
    const TYPE = 'random_block';
    const RESOURCE = RandomResource::class;

    protected $table = 'block_randoms';

    protected $with = [
        'variants'
    ];

    public function variants()
    {
        return $this->hasMany(Variant::class, 'block_id');
    }

    /**
     * @param Play $play
     * @return null
     * @throws \Exception
     */
    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        $variants = $this->getAttribute('variants');

        if (!count($variants)) {
            throw new \Exception('No variants in random block');
        }

        $i = random_int(0, count($variants) - 1);

        $nextBlock = $variants[$i]->next_block;

        if (!$nextBlock) {
            throw new \Exception('Not setted next block in random[' . $i . ']');
        }

        return $nextBlock;
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function updateBlock($data)
    {
        if (key_exists('new_variant', $data)) {
            $this->extraBlockUpdated['variant_created'] = $this->createNewVariant();
        }

        if (key_exists('delete_variant', $data)) {
            $this->extraBlockUpdated['variant_deleted'] = $this->deleteVariant();
        }

        if (key_exists('update_variant', $data)) {
            $this->extraBlockUpdated['variant_updated'] = $this->updateVariant($data['update_variant']);
        }

        return $this->extraBlockUpdated;
    }

    public function createNewVariant()
    {
        $variant = Random\Variant::create([
            'block_id' => $this->getAttribute('id')
        ]);

        return new RandomVariantResource($variant->fresh());
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function updateVariant($data)
    {
        $variant = Random\Variant::findOrFail(
            $data['variant']
        );

        if ($variant->getAttribute('block_id') !== $this->getAttribute('id')) {
            throw new \Exception('This variant not belongs to current block');
        }

        if (key_exists('next_block', $data)) {
            if ($data['next_block'] == null) {
                $variant->setAttribute('next_block', null);
            } else {
                $nextBlock = Block::findOrFail(
                    $data['next_block']
                );

                if ($nextBlock->getAttribute('game_id') != $this->getAttribute('block')->game_id) {
                    throw new \Exception('This block not accepted to current game');
                }

                $variant->setAttribute('next_block', $nextBlock->getAttribute('id'));
            }
        }

        return $variant->save();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function deleteVariant()
    {
        $variant = $this->variants()->orderBy('created_at', 'desc')->first();

        if (!$variant) {
            throw new \Exception('There is no variants');
        }

        return $variant->delete();
    }
}
