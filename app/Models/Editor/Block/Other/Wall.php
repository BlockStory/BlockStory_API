<?php

namespace App\Models\Editor\Block\Other;

use App\Http\Resources\API\V1\Blocks\WallResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;

class Wall extends BaseBlock
{
    const TYPE = 'wall_block';
    const RESOURCE = WallResource::class;

    protected $table = 'block_walls';

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        return $this->getAttribute('next_block');
    }

    public function updateBlock($data)
    {
        // TODO: Implement updateBlock() method.
    }
}
