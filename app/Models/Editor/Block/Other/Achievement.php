<?php

namespace App\Models\Editor\Block\Other;

use App\Http\Resources\API\V1\Blocks\AchievementResource;
use App\Models\Editor\BaseBlock;
use App\Models\Play\Play;

class Achievement extends BaseBlock
{
    const TYPE = 'block_achievement';
    const RESOURCE = AchievementResource::class;

    protected $table = 'block_achievements';

    public function renderBlock(Play &$play)
    {
        parent::renderBlock($play);

        return $this->getAttribute('next_block');
    }

    public function updateBlock($data)
    {
        // TODO: Implement updateBlock() method.
    }
}
