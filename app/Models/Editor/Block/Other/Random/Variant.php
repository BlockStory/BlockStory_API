<?php

namespace App\Models\Editor\Block\Other\Random;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $table = 'block_random_variants';

    protected $guarded = [];
}
