<?php

namespace App\Models\User;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Confirmation extends Model
{
    const TYPE_USER_CREATED = 'user_created';

    const TYPES = [
      self::TYPE_USER_CREATED
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Confirmation $confirmation) {
           $confirmation->code = Str::uuid();
           $confirmation->hash = $confirmation->toHash();
        });
    }

    protected $table = 'user_confirmations';

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'expired_at',
        'accepted_at'
    ];

    public function url()
    {
        return config('app.front_url') . '/confirm/'
            . '?type=' . $this->type
            . '&code=' . $this->code
            . '&hash=' . $this->hash;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toHash($type = null, $code = null) {

        $type = $type ?: $this->type;
        $code = $code ?: $this->code;

        return md5($code . $type . md5($code . $type . md5($type . $code)));
    }
}
