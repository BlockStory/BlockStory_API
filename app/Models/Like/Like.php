<?php

namespace App\Models\Like;

use App\Models\Comment\Comment;
use App\Models\Game\Game;
use App\Models\Post\Post;
use App\Modules\IdUUID;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use IdUUID;

    const MODEL_TYPES = [
        'post' => Post::class,
        'game' => Game::class,
        'comment' => Comment::class,
    ];

    public $incrementing = false;

    protected $guarded = [];

    protected $with = ['user'];

    public function likeable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
