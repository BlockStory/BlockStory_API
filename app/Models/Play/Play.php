<?php

namespace App\Models\Play;

use App\Models\Editor\Block\Block;
use App\Models\Game\Game;
use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;

class Play extends Model
{
    use IdUUID;

    const MAX_STEPS = 20;

    public static $PLAY_STEP = 0;

    public $incrementing = false;

    protected $guarded = [];

    public function params()
    {
        return $this->hasMany(Param::class);
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function block()
    {
        return $this->belongsTo(Block::class, 'current_block_id');
    }

    /**
     * @param null $nextBlockId
     * @return $this
     * @throws \Exception
     */
    public function goNext($nextBlockId = null)
    {
        \DB::beginTransaction();

        try {
            do{
                if (self::$PLAY_STEP > self::MAX_STEPS) {
                    throw new \Exception('Max steps limiter');
                }

                $block = $this->getAttribute('block');

                if ($nextBlockId) {
                    $block->getAttribute('blockable')->beforeExitRender($this);

                    $block = Block::findOrFail($nextBlockId);

                    if ($block->getAttribute('game_id') !== $this->getAttribute('game_id')) {
                        throw new \Exception('Block not in that game');
                    }

                    $this->setAttribute('current_block_id', $block->getAttribute('id'));
                } else {
                    $this->load('block.blockable');
                    $block = $this->getAttribute('block');
                }

                if (!$block) {
                    throw new \Exception('Block not founded');
                }

                $blockable = $block->getAttribute('blockable');

                $this->setAttribute('current_block_id', $block->id);

                $blockable->beforeRender($this);

                $nextBlockId = $blockable->renderBlock($this);

                self::$PLAY_STEP++;
            } while ($nextBlockId);

            $this->save();

            \DB::commit();

            return $this->fresh([
                'block.blockable',
                'game' => function (?BelongsTo $query) {
                    $query->without('likes');
                },
                'params'
            ]);
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
