<?php

namespace App\Models\Play;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;
use App\Models\Game\Param as GameParam;

class Param extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $table = 'play_params';

    protected $guarded = [];

    protected $with = ['baseParam'];

    public function baseParam()
    {
        return $this->belongsTo(GameParam::class, 'param_id');
    }
}
