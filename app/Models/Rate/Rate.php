<?php

namespace App\Models\Rate;

use App\Modules\IdUUID;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $guarded = [];

    public function rateable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
