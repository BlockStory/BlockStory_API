<?php

namespace App\Models\Game;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    use IdUUID;

    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_BOOLEAN = 'boolean';

    const TYPES = [
        self::TYPE_STRING, self::TYPE_INTEGER, self::TYPE_BOOLEAN
    ];

    public $incrementing = false;
    protected $guarded = [];

    protected $casts = [
        'is_item' => 'boolean',
        'item_always_visible' => 'boolean'
    ];
}
