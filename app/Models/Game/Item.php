<?php

namespace App\Models\Game;

use App\Modules\IdUUID;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use IdUUID;

    public $incrementing = false;

    protected $guarded = [];

    protected $casts = [
        'is_unique' => 'boolean',
        'always_visible' => 'boolean'
    ];
}
