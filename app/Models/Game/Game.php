<?php

namespace App\Models\Game;

use App\Models\Editor\Chapter;
use App\Models\Play\Play;
use App\Models\Tag\Tag;
use App\Modules\Commentable;
use App\Modules\IdUUID;
use App\Modules\Likeable;
use App\Modules\Rateable;
use App\Modules\Reviewable;
use App\Modules\Taggable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use IdUUID, Likeable, Commentable, Taggable, SoftDeletes, Rateable, Reviewable;

    public $incrementing = false;

    protected $guarded = [];

    protected $with = ['likes'];

    protected $casts = [
        'allow_comments' => 'boolean',
        'published' => 'boolean',
        'sandbox' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function plays()
    {
        return $this->hasMany(Play::class);
    }

    public function starterChapter()
    {
        return $this->belongsTo(Chapter::class, 'starter_chapter_id');
    }

    public function params()
    {
        return $this->hasMany(Param::class);
    }
}
