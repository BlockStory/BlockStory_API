<?php

namespace App\Exceptions\API\V1;

use Exception;

class ApiBlockUpdateException extends Exception
{
    public $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function render()
    {
        return $this->response;
    }
}
