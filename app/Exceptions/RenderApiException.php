<?php

namespace App\Exceptions;

use Exception;

class RenderApiException extends Exception
{
    public $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function render()
    {
        return $this->response;
    }
}
