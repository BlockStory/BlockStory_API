<?php

namespace App\Http\Middleware;

use App\Exceptions\RenderApiException;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Closure;
use Auth;
use Illuminate\Http\Response;

class CheckCanEditGame
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws RenderApiException
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            throw new RenderApiException(response([
                'Auth' => 'User can\'t edit this game',
                'status' => StatusCode::FORBIDDEN
            ], Response::HTTP_FORBIDDEN));
        }

        $gameId = $request->route('game');

        $game = Game::findOrFail($gameId);

        if ($game->user_id !== Auth::id()) {
            throw new RenderApiException(response([
                'Auth' => 'User can\'t edit this game',
                'status' => StatusCode::FORBIDDEN
            ], Response::HTTP_FORBIDDEN));
        }

        return $next($request);
    }
}
