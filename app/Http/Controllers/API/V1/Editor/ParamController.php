<?php

namespace App\Http\Controllers\API\V1\Editor;

use App\Http\Controllers\API\V1\ApiBaseController;
use App\Http\Requests\API\V1\Editor\Param\{Delete, Store, Update};

class ParamController extends ApiBaseController
{
    public function index()
    {

    }

    public function show()
    {

    }

    public function store(Store $request)
    {
        return $request->execute();
    }

    public function update(Update $request)
    {
        return $request->execute();
    }

    public function delete(Delete $request)
    {
        return $request->execute();
    }
}
