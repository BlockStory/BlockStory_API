<?php

namespace App\Http\Controllers\API\V1\Editor;

use App\Http\Controllers\API\V1\ApiBaseController;
use App\Http\Requests\API\V1\Editor\Show;

class EditorController extends ApiBaseController
{
    public function show(Show $request)
    {
        return $request->execute();
    }
}
