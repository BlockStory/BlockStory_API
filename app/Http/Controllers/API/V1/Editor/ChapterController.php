<?php

namespace App\Http\Controllers\API\V1\Editor;

use App\Http\Controllers\API\V1\ApiBaseController;
use App\Http\Requests\API\V1\Editor\Chapter\Show;
use App\Http\Requests\API\V1\Editor\Chapter\Store;
use App\Http\Requests\API\V1\Editor\Chapter\Update;

class ChapterController extends ApiBaseController
{
    public function show(Show $request)
    {
        return $request->execute();
    }

    public function store(Store $request)
    {
        return $request->execute();
    }

    /**
     * @param Update $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Update $request)
    {
        return $request->execute();
    }
}
