<?php

namespace App\Http\Controllers\API\V1\Editor;

use App\Http\Controllers\API\V1\ApiBaseController;
use App\Http\Requests\API\V1\Editor\Item\{
    Delete, Show, Store, Update, Index
};

class ItemController extends ApiBaseController
{
    public function index(Index $request)
    {
        return $request->execute();
    }

    public function store(Store $request)
    {
        return $request->execute();
    }

    public function show(Show $request)
    {
        return $request->execute();
    }

    public function update(Update $request)
    {
        return $request->execute();
    }

    public function delete(Delete $request)
    {
        return $request->execute();
    }
}
