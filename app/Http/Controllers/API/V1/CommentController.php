<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Comment\Like\Create as StoreLike;
use App\Http\Requests\API\V1\Comment\Like\Remove as RemoveLike;
use App\Http\Requests\API\V1\Comment\Show;
use App\Http\Requests\API\V1\Comment\Store;
use App\Http\Requests\API\V1\Comment\Remove;

class CommentController extends ApiBaseController
{
    public function show(Show $request)
    {
        return $request->execute();
    }

    public function store(Store $request)
    {
        return $request->execute();
    }

    public function storeLike(StoreLike $request)
    {
        return $request->execute();
    }

    public function removeLike(RemoveLike $request)
    {
        return $request->execute();
    }

    public function delete(Remove $request)
    {
        return $request->execute();
    }
}
