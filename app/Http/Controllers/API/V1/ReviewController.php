<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Review\Show;
use App\Http\Requests\API\V1\Review\Comment\{
    Store as StoreComment,
    Show as ShowComments,
    Remove as RemoveComment
};
use App\Http\Requests\API\V1\Review\Like\{
    Store as StoreLike,
    Remove as RemoveLike
};
use App\Http\Requests\API\V1\Review\Update;

class ReviewController extends ApiBaseController
{
    public function index()
    {
        //
    }

    public function store()
    {
        //
    }

    /**
     * @param Show $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function show(Show $request)
    {
        return $request->execute();
    }

    public function update(Update $request)
    {
        return $request->execute();
    }

    public function destroy()
    {
        //
    }

    public function showComments(ShowComments $request)
    {
        return $request->execute();
    }

    public function storeComment(StoreComment $request)
    {
        return $request->execute();
    }

    public function removeComment(RemoveComment $request)
    {
        return $request->execute();
    }

    public function storeLike(StoreLike $request)
    {
        return $request->execute();
    }

    public function removeLike(RemoveLike $request)
    {
        return $request->execute();
    }
}
