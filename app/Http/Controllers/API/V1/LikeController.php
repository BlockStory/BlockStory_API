<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Like\{Remove, Store};

class LikeController extends ApiBaseController
{
    /**
     * @param Store $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Store $request)
    {
        return $request->execute();
    }

    /**
     * @param Remove $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Remove $request)
    {
        return $request->execute();
    }
}
