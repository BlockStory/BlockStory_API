<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Confirm\Confirm;

class ConfirmController extends ApiBaseController
{
    /**
     * @param Confirm $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function confirm(Confirm $request)
    {
        return $request->execute();
    }
}
