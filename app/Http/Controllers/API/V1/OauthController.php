<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Auth\Show;
use App\Modules\Output;
use App\Modules\StatusCode;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;

class OauthController extends AccessTokenController
{
    use Output;

    public function issueUserToken(ServerRequestInterface $request)
    {
        if (\request()->get('grant_type') == 'password') {

            $validator = \Validator::make(\request()->all(), [
                'grant_type' => 'required|string|in:password',
                'username' => 'required|string|email|exists:users,email',
                'password' => 'required|string'
            ]);

            if ($validator->fails()) {
                return $this->setErrorValidatorEndRender($validator);
            }

            $user = User::where('email', \request()->get('username'))->first();

            if (!$user) {
                return $this->setErrorEndRender([
                    'Not founded'
                ], StatusCode::NOT_FOUNDED);
            }

            if (!$user->email_confirmed) {
                return $this->setErrorEndRender([
                    'Not confirmed email'
                ], StatusCode::USER_NOT_CONFIRMED_EMAIL);
            }
        }

        return $this->issueToken($request);
    }
}
