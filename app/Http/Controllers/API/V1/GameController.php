<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Game\{
    Create, Index, Show, Update
};
use App\Http\Requests\API\V1\Game\Rate\{
    Show as ShowRate,
    Create as CreateRate
};
use App\Http\Requests\API\V1\Game\Comment\{
    Store as StoreComment,
    Show as ShowComment,
    Remove as RemoveComment
};
use App\Http\Requests\API\V1\Game\Like\{
    Store as StoreLike,
    Remove as RemoveLike
};

use App\Http\Requests\API\V1\Game\Review\{
    Store as StoreReview,
    Show as ShowReview
};

class GameController extends ApiBaseController
{
    public function index(Index $request)
    {
        return $request->execute();
    }

    public function store(Create $request)
    {
        return $request->execute();
    }

    /**
     * @param Show $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function show(Show $request)
    {
        return $request->execute();
    }

    /**
     * @param Update $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Update $request)
    {
        return $request->execute();
    }

    public function destroy()
    {
        //
    }

    public function getRates(ShowRate $request)
    {
        return $request->execute();
    }

    public function storeRate(CreateRate $request)
    {
        return $request->execute();
    }

    public function getComments(ShowComment $request)
    {
        return $request->execute();
    }

    public function storeComment(StoreComment $request)
    {
        return $request->execute();
    }

    /**
     * @param RemoveComment $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function deleteComment(RemoveComment $request)
    {
        return $request->execute();
    }

    public function storeLike(StoreLike $request)
    {
        return $request->execute();
    }

    public function removeLike(RemoveLike $request)
    {
        return $request->execute();
    }

    public function storeReview(StoreReview $request)
    {
        return $request->execute();
    }

    public function getReviews(ShowReview $request)
    {
        return $request->execute();
    }
}
