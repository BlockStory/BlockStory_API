<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Auth\Show;
use App\Modules\Output;

class AuthController extends ApiBaseController
{
    use Output;

    public function getAuthUser(Show $request)
    {
        return $request->execute();
    }
}
