<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\User\Show;
use App\Http\Requests\API\V1\User\ShowGames;
use App\Http\Requests\API\V1\User\ShowPosts;
use App\Http\Requests\API\V1\User\ShowReviews;
use App\Http\Requests\API\V1\User\Store;
use App\Http\Requests\API\V1\User\Update;

class UserController extends ApiBaseController
{
    public function index()
    {
        //
    }

    public function store(Store $request)
    {
        return $request->execute();
    }

    public function show(Show $request)
    {
        return $request->execute();
    }

    public function update(Update $request)
    {
        return $request->execute();
    }

    public function destroy($id)
    {
        //
    }

    public function showPosts(ShowPosts $request)
    {
        return $request->execute();
    }

    public function showGames(ShowGames $request)
    {
        return $request->execute();
    }

    public function showReviews(ShowReviews $request)
    {
        return $request->execute();
    }
}
