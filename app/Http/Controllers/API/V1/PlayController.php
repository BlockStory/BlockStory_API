<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Play\{
    Index, Store, Show, Update, Destroy
};

class PlayController extends ApiBaseController
{
    public function index(Index $request)
    {
        return $request->execute();
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Store $request)
    {
        return $request->execute();
    }

    public function show(Show $request)
    {
        return $request->execute();
    }

    public function update(Update $request)
    {
        return $request->execute();
    }

    public function destroy(Destroy $request)
    {
        return $request->execute();
    }
}
