<?php


namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;

class ApiBaseController extends Controller
{
    public function __construct()
    {
        \Auth::shouldUse('api');
    }
}