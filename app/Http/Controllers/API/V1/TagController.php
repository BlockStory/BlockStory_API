<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Tag\Create;
use App\Http\Requests\API\V1\Tag\Search;
use Illuminate\Http\Request;

class TagController extends ApiBaseController
{
    public function index()
    {
        //
    }

    public function search(Search $request)
    {
        return $request->execute();
    }

    public function store(Create $request)
    {
        return $request->execute();
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
