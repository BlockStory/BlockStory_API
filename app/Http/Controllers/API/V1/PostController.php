<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\Post\Create;
use App\Http\Requests\API\V1\Post\Index;
use App\Http\Requests\API\V1\Post\Show;
use App\Http\Requests\API\V1\Post\Update;

class PostController extends ApiBaseController
{
    public function index(Index $request)
    {
        return $request->execute();
    }

    /**
     * @param Create $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {
        return $request->execute();
    }

    /**
     * @param Show $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function show(Show $request)
    {
        return $request->execute();
    }

    /**
     * @param Update $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\RenderApiException
     */
    public function update(Update $request)
    {
        return $request->execute();
    }

    public function destroy($id)
    {
        //
    }
}
