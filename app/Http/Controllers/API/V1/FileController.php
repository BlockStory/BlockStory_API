<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\File\Upload\Image;

class FileController extends ApiBaseController
{
    public function uploadImage(Image $request)
    {
        return $request->execute();
    }
}
