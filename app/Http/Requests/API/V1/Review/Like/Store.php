<?php

namespace App\Http\Requests\API\V1\Review\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Post\Post;
use App\Models\Review\Review;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-review-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $review = Review::findOrFail(
            $this->route('review')
        );

        if ($review->addLike()) {
            return $this->setDataEndRender([
                'Created' => true
            ], StatusCode::CREATED);
        }

        return $this->setErrorEndRender([
            'Created' => 'Like was not created'
        ], StatusCode::NOT_ALLOWED);
    }
}
