<?php

namespace App\Http\Requests\API\V1\Review\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Post\Post;
use App\Models\Review\Review;
use App\Modules\StatusCode;
use Auth;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-review-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $review = Review::findOrFail(
            $this->route('review')
        );

        if ($review->removeLike()) {
            return $this->setDataEndRender([
                'Deleted' => true
            ], StatusCode::UPDATED);
        }

        return $this->setErrorEndRender([
            'Deleted' => 'Like was not deleted'
        ], StatusCode::NOT_ALLOWED);
    }
}
