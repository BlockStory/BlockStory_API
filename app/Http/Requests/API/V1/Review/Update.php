<?php

namespace App\Http\Requests\API\V1\Review;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Review\Review;
use App\Modules\StatusCode;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Response;

class Update extends ApiFormRequest
{
    public $review;

    public function authorize()
    {
        $this->review = Review::findOrFail(
            $this->route('review')
        );

        return Auth::check()
            && Auth::user()->can('update-review')
            && (Auth::user()->owns($this->review) || Auth::user()->can('update-review-all'));
    }

    public function rules()
    {
        return [
            'title'     => 'sometimes|string|max:128',
            'text'      => 'sometimes|required|string',
            'published' => 'sometimes|required|boolean',
            'allow_comments'  => 'sometimes|required|boolean'
        ];
    }

    public function execute()
    {
        $safeParams = [
            'title', 'text', 'allow_comments', 'published'
        ];

        foreach ($safeParams as $param) {
            if ($this->exists($param)) {
                $this->review->{$param} = $this->get($param);
            }
        }

        if ($this->review->isDirty('published')) {
            if ($this->review->published) {
                $this->review->user->increment('total_reviews');
                $this->review->reviewable->increment('total_reviews');
                if (!$this->review->published_at) {
                    $this->review->published_at = Carbon::now()->toDateTimeString();
                }
            } else {
                $this->review->user->decrement('total_reviews');
                $this->review->reviewable->decrement('total_reviews');
            }
        }

        $dirty = $this->review->getDirty();

        $this->review->save();

        return $this->setDataEndRender([
            'Updated' => $dirty,
        ], StatusCode::UPDATED, Response::HTTP_ACCEPTED);
    }
}
