<?php

namespace App\Http\Requests\API\V1\Review\Comment;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-review-comment');
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws RenderApiException
     */
    public function execute()
    {
        $comment = Comment::findOrFail(
            $this->route('comment')
        );

        if (!Auth::user()->owns($comment)) {
            if (!Auth::user()->can('delete-review-comment-all')) {
                throw new RenderApiException($this->setErrorEndRender([
                    'Forbidden' => 'User can\'t delete comment'
                ], StatusCode::FORBIDDEN, Response::HTTP_FORBIDDEN));
            }
        }

        $review = $comment->commentable->fresh();

        return $this->setDataEndRender([
            'Deleted' => $review->deleteComment($comment)
        ], StatusCode::DELETED);
    }
}
