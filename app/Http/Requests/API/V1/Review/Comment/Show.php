<?php

namespace App\Http\Requests\API\V1\Review\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Review\Review;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $review = Review::findOrFail(
            $this->route('review')
        );

        $review = $review->load('comments.user');

        return $this->setDataEndRender([
            'comments' => CommentResource::collection($review->comments)
        ]);
    }
}
