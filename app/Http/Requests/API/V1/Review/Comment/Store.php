<?php

namespace App\Http\Requests\API\V1\Review\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Review\Review;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-review-comment');
    }

    public function rules()
    {
        return [
            'text' => 'required|string|max:255'
        ];
    }

    public function execute()
    {
        $review = Review::findOrFail(
            $this->route('review')
        );

        $comment = $review->addComment($this->get('text'));

        $comment->load('user');

        return $this->setDataEndRender([
            'comment' => new CommentResource($comment)
        ], StatusCode::CREATED);
    }
}
