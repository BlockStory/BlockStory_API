<?php

namespace App\Http\Requests\API\V1\Review;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ReviewResource;
use App\Models\Review\Review;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @throws RenderApiException
     */
    public function execute()
    {
        $review = Review::findOrFail(
            $this->route('review')
        );

        if (!$review->published) {
            if (!Auth::check() || (!Auth::user()->owns($review) && !Auth::user()->can('show-review-all'))) {
                throw new RenderApiException(
                    $this->setErrorEndRender([
                        'Authorization' => 'User can\'t see this review'
                    ],
                        StatusCode::FORBIDDEN,
                        Response::HTTP_FORBIDDEN
                    )
                );
            }
        }

        if ($this->exists('with')) {
            $with = $this->get('with');

            if (in_array('user', $with)) {
                $review->load('user');
            }

            if (in_array('reviewable', $with)) {
                $review->load('reviewable');
            }
        }

        return $this->setDataEndRender([
            'review' => new ReviewResource($review)
        ]);
    }
}
