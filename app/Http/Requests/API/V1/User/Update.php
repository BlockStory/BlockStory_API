<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Modules\StatusCode;
use App\User;

class Update extends ApiFormRequest
{
    private $user;

    public function authorize()
    {
        $this->user = User::findOrFail($this->route('user'));

        return \Auth::user()->owns($this->user, 'id');
    }

    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|min:1|max:128',
            'slug' => 'sometimes|required|string|min:1|max:32|alpha_dash|unique:users,slug',
            'slogan' => 'sometimes|nullable|string',
            'description' => 'sometimes|nullable|string',
            'link_vk' => 'sometimes|nullable|url',
            'link_fb' => 'sometimes|nullable|url',
            'link_email' => 'sometimes|nullable|email',
            'avatar' => 'sometimes|nullable|url',
            'cover' => 'sometimes|nullable|url',
            'password' => 'sometimes|required|string|min:4',
            'password_old' => 'required_with:password|string'
        ];
    }

    public function execute()
    {
        $updated = [];

        $unsafe = [
            'name', 'slug', 'slogan', 'description',
            'link_vk', 'link_fb', 'link_email',
            'avatar', 'cover'
        ];

        foreach ($unsafe as $item) {
            if ($this->exists($item)) {
                $this->user->{$item} = $this->get($item);
                $updated[] = $item;
            }
        }

        if ($this->user->save()) {
            return $this->setDataEndRender([
                'updated' => $updated
            ], StatusCode::UPDATED);
        }
    }
}
