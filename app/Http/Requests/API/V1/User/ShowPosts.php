<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PostCollection;
use App\Http\Resources\API\V1\PostResource;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use App\User;

class ShowPosts extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $user = User::findOrFail(
            $this->route('user')
        );

        $posts = $user->posts();

        $posts->when($this->exists('with') && in_array('user', $this->get('with')), function ($q) {
            return $q->with('user');
        });

        return PostResource::collection($posts->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
