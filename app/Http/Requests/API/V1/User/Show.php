<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\UserResource;
use App\User;
use Illuminate\Support\Facades\Auth;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return \Auth::check();
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $user = User::findOrFail(
            $this->route('user')
        );

        return $this->setDataEndRender([
            'user' => new UserResource($user)
        ]);
    }
}
