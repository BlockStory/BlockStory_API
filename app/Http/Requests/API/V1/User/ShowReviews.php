<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ReviewResource;
use App\Modules\StatusCode;
use App\User;
use Auth;

class ShowReviews extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $user = User::findOrFail(
            $this->route('user')
        );

        $reviews = $user->reviews();

        $reviews->when($this->exists('with') && in_array('user', $this->get('with')), function ($q) {
            return $q->with('user');
        });

        return ReviewResource::collection($reviews->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
