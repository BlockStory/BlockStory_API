<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\UserResource;
use App\Modules\StatusCode;
use App\User;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => [
                'required', 'string', 'email', 'unique:users,email'
            ],
            'password' => [
                'required', 'string', 'min:4'
            ],
            'name' => [
                'required', 'string', 'min:1'
            ]
        ];
    }

    public function execute()
    {
        $user = User::create([
            'email' => $this->get('email'),
            'password' => $this->get('password'),
            'name' => $this->get('name')
        ]);

        return $this->setDataEndRender([
            'user' => new UserResource($user)
        ], StatusCode::CREATED);
    }
}
