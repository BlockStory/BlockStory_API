<?php

namespace App\Http\Requests\API\V1\User;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\GameResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use App\User;

class ShowGames extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $user = User::findOrFail(
            $this->route('user')
        );

        $games = $user->games();

        $games->when($this->exists('with') && in_array('user', $this->get('with')), function ($q) {
            return $q->with('user');
        });

        return GameResource::collection($games->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
