<?php

namespace App\Http\Requests\API\V1;

interface ApiFormRequestInterface
{
    public function execute();
}