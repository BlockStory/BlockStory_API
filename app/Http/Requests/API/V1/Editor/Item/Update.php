<?php

namespace App\Http\Requests\API\V1\Editor\Item;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ItemResource;
use App\Models\Game\Game;
use App\Models\Game\Item;
use App\Modules\StatusCode;

class Update extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|min:1|max:32',
            'description' => 'nullable|string',
            'default' => 'required|integer',
            'change_image' => 'required|boolean',
            'image' => 'nullable|url',
            'always_visible' => 'required|boolean',
            'is_unique' => 'required|boolean'
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $item = Item::findOrFail(
            $this->route('item')
        );

        if ($item->getAttribute('game_id') !== $game->getAttribute('id')) {
            throw new \Exception('Item not belong to cuurent game');
        }

        $forUpdate = [
            'title', 'description', 'default',
            'always_visible', 'is_unique'
        ];

        foreach ($forUpdate as $value) {
            $item->setAttribute($value, $this->get($value));
        }

        if ($this->get('change_image')) {
            $item->setAttribute('image', $this->get('image'));
        }

        return $this->setDataEndRender([
            'updated' => $item->save(),
            'item' => new ItemResource($item->fresh())
        ], StatusCode::UPDATED);
    }
}
