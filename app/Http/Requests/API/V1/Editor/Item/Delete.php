<?php

namespace App\Http\Requests\API\V1\Editor\Item;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Game\Game;
use App\Models\Game\Item;
use App\Modules\StatusCode;
use Illuminate\Http\Response;

class Delete extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $item = Item::findOrFail(
            $this->route('item')
        );

        if ($item->getAttribute('game_id') !== $game->getAttribute('id')) {
            return $this->setErrorEndRender([
                'Error' => 'Param not belong to game'
            ], StatusCode::NOT_ALLOWED, Response::HTTP_BAD_REQUEST);
        }

        return $this->setDataEndRender([
            'Deleted' => $item->delete()
        ], StatusCode::DELETED);
    }
}
