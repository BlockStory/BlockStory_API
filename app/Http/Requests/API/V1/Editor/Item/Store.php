<?php

namespace App\Http\Requests\API\V1\Editor\Item;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ItemResource;
use App\Models\Game\Item;
use App\Modules\StatusCode;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|min:1|max:32',
            'description' => 'nullable|string',
            'default' => 'required|integer',
            'image' => 'nullable|url',
            'always_visible' => 'required|boolean',
            'is_unique' => 'required|boolean'
        ];
    }

    public function execute()
    {
        $item = Item::create([
            'game_id' => $this->route('game'),
            'title' => $this->get('title'),
            'description' => $this->get('description'),
            'default' => $this->get('default'),
            'image' => $this->get('image'),
            'always_visible' => $this->get('always_visible'),
            'is_unique' => $this->get('is_unique')
        ]);

        return $this->setDataEndRender([
            'item' => new ItemResource($item)
        ], StatusCode::CREATED);
    }
}
