<?php

namespace App\Http\Requests\API\V1\Editor\Param;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ParamResource;
use App\Models\Game\Game;
use App\Models\Game\Param;
use App\Modules\StatusCode;
use App\Rules\Uppercase;
use Illuminate\Http\Response;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required', 'string', 'alpha_dash', new Uppercase
            ],
            'type'  => 'required|string|in:' . implode(',', Param::TYPES),
            'regex' => 'nullable|string',
            'default' => 'nullable|string',
            'is_item' => 'required|boolean',
            'item_title' => 'required_if:is_item,true|string',
            'item_description' => 'sometimes|nullable|string',
            'item_image' => 'sometimes|nullable|url',
            'item_always_visible' => 'required_if:is_item,true|boolean'
        ];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        if (Param::all()->where('game_id', $game->getAttribute('id'))->where('name', $this->get('name'))->first()) {
            return $this->setErrorEndRender([
                'Error' => 'Param exists'
            ], StatusCode::NOT_ALLOWED, Response::HTTP_BAD_REQUEST);
        }

        $data = [
            'game_id'       => $game->getAttribute('id') ,
            'name'          => $this->get('name'),
            'type'          => $this->get('type'),
            'regex_rule'    => $this->get('regex'),
            'default'       => $this->get('default'),
            'is_item'       => $this->get('is_item'),
        ];

        if ($this->get('is_item')) {
            $data += [
                'item_title'            => $this->get('item_title'),
                'item_description'      => $this->get('item_description'),
                'item_image'            => $this->get('item_image'),
                'item_always_visible'   => $this->get('item_always_visible')
            ];
        }

        $param = Param::create($data);

        return $this->setDataEndRender([
            'param' => new ParamResource($param)
        ], StatusCode::CREATED);
    }
}
