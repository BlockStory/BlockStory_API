<?php

namespace App\Http\Requests\API\V1\Editor\Param;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ParamResource;
use App\Models\Game\Game;
use App\Models\Game\Param;
use App\Modules\StatusCode;
use App\Rules\Uppercase;
use Illuminate\Http\Response;

class Update extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required', 'string', 'alpha_dash', new Uppercase
            ],
            'type'  => 'required|string|in:' . implode(',', Param::TYPES),
            'regex_rule' => 'nullable|string',
            'default' => 'nullable|string',
            'is_item' => 'required|boolean',
            'item_title' => 'required_if:is_item,true|string',
            'item_description' => 'sometimes|nullable|string',
            'item_image' => 'sometimes|nullable|url',
            'item_always_visible' => 'required_if:is_item,true|boolean',
            'change_image' => 'required_if:is_item,true|boolean'
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $param = Param::findOrFail(
            $this->route('param')
        );

        if ($param->getAttribute('game_id') !== $game->getAttribute('id')) {
            throw new \Exception('Item not belong to cuurent game');
        }

        if ($param->getAttribute('name') != $this->get('name')) {
            if (Param::all()->where('game_id', $game->getAttribute('id'))->where('name', $this->get('name'))->first()) {
                return $this->setErrorEndRender([
                    'Error' => 'Param exists'
                ], StatusCode::NOT_ALLOWED, Response::HTTP_BAD_REQUEST);
            }
        }

        $safeUpdate = ['name', 'type', 'regex_rule', 'default', 'is_item'];

        if ($this->get('is_item')) {
            $safeUpdate = array_merge($safeUpdate, ['item_title', 'item_description', 'item_always_visible']);

            if ($this->get('change_image')) {
                $safeUpdate = array_merge($safeUpdate, ['item_image']);
            }
        }

        foreach ($safeUpdate as $value) {
            $param->setAttribute($value, $this->get($value));
        }

        return $this->setDataEndRender([
            'updated' => $param->save(),
            'param' => new ParamResource($param->fresh())
        ], StatusCode::UPDATED);
    }
}
