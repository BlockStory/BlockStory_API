<?php

namespace App\Http\Requests\API\V1\Editor;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ChapterResource;
use App\Http\Resources\API\V1\GameResource;
use App\Http\Resources\API\V1\ItemResource;
use App\Http\Resources\API\V1\ParamResource;
use App\Models\Editor\Chapter;
use App\Models\Game\Game;
use App\Models\Game\Item;
use App\Models\Game\Param;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $game->load('starterChapter');

        return $this->setDataEndRender([
            'chapters' => ChapterResource::collection(
                Chapter::where('game_id', $game->getAttribute('id'))->get()
            ),
            'params' => ParamResource::collection(
                Param::where('game_id', $game->getAttribute('id'))->get()
            ),
            'items' => ItemResource::collection(
                Item::where('game_id', $game->getAttribute('id'))->get()
            ),
            'game' => new GameResource($game)
        ]);
    }
}
