<?php

namespace App\Http\Requests\API\V1\Editor\Chapter;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Editor\Block\Block;
use App\Models\Editor\Chapter;
use App\Modules\StatusCode;

class Update extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'starter_block' => 'sometimes|nullable|string'
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        $chapter = Chapter::findOrFail(
            $this->route('chapter')
        );

        if ($this->exists('starter_block')) {

            if ($this->get('starter_block') == null) {
                $chapter->setAttribute('starter_block_id', null);
            } else {
                $block = Block::findOrFail(
                    $this->get('starter_block')
                );

                if ($block->getAttribute('chapter_id') != $chapter->getAttribute('id')) {
                    throw new \Exception('Block not belong to chapter');
                }

                $chapter->setAttribute('starter_block_id', $block->getAttribute('id'));
            }
        }


        return $this->setDataEndRender([
            'updated' => $chapter->save()
        ], StatusCode::UPDATED);
    }
}
