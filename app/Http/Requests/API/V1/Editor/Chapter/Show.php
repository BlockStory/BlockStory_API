<?php

namespace App\Http\Requests\API\V1\Editor\Chapter;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ChapterResource;
use App\Models\Editor\Chapter;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $chapter = Chapter::findOrFail(
            $this->route('chapter')
        );

        return $this->setDataEndRender([
            'chapter' => new ChapterResource($chapter)
        ]);
    }
}
