<?php

namespace App\Http\Requests\API\V1\Editor\Chapter;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ChapterResource;
use App\Models\Editor\Chapter;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|min:1|max:255'
        ];
    }

    public function execute()
    {
        $chapter = Chapter::create([
            'user_id' => Auth::id(),
            'game_id' => $this->route('game'),
            'title' => $this->get('title')
        ]);

        return $this->setDataEndRender([
            'chapter' => new ChapterResource($chapter)
        ], StatusCode::CREATED);
    }
}
