<?php

namespace App\Http\Requests\API\V1\Editor\Block;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\BlockResource;
use App\Models\Editor\Block\Block;
use App\Models\Editor\Chapter;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Illuminate\Http\Response;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chapter' => 'required|exists:chapters,id',
            'type' => 'required|in:' . implode(',', array_keys(config('blocks.types'))),
            'left' => 'required|integer|min:0',
            'top' => 'required|integer|min:0'
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $chapter = Chapter::findOrFail(
            $this->get('chapter')
        );

        if ($chapter->getAttribute('game_id') !== $game->getAttribute('id')) {
            return $this->setErrorEndRender([
                'Editor' => 'Chapter not belong to game'
            ], StatusCode::NOT_ALLOWED, Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $block = Block::createBlock($this->get('type'), $game, $chapter, [
            'top' => $this->get('top'),
            'left' => $this->get('left')
        ]);

        return $this->setDataEndRender([
            'block' => new BlockResource($block)
        ], StatusCode::CREATED);
    }
}
