<?php

namespace App\Http\Requests\API\V1\Editor\Block;

use App\Exceptions\API\V1\ApiBlockUpdateException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Editor\Block\Block;
use App\Modules\StatusCode;
use Illuminate\Http\Response;

class Update extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|required|string|min:1',
            'position_left' => 'sometimes|required|integer|min:0',
            'position_top' => 'sometimes|required|integer|min:0',
            'data' => 'sometimes|array'
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function execute()
    {
        $block = Block::findOrFail(
            $this->route('block')
        );

        $safeParams = [
            'title', 'position_left', 'position_top'
        ];

        foreach ($safeParams as $param) {
            if ($this->exists($param)) {
                $block->{$param} = $this->get($param);
            }
        }

        \DB::beginTransaction();

        try {
            if ($this->exists('data')) {

                $data = $this->get('data');

                $extra = $block->getAttribute('blockable')->updateBlock($data);
            }

            $dirty = $block->getDirty();

            $block->save();

            \DB::commit();

            return $this->setDataEndRender([
                'Updated' => $dirty,
                'extra' => $extra ?? ''
            ], StatusCode::UPDATED, Response::HTTP_ACCEPTED);

        } catch (ApiBlockUpdateException $exception) {
            \DB::rollBack();
            throw $exception;
        } catch (\Exception $exception) {
            \DB::rollBack();

            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
