<?php

namespace App\Http\Requests\API\V1\Editor\Block;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Editor\Block\Block;
use App\Models\Game\Game;
use App\Modules\StatusCode;

class Delete extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response|int
     * @throws \Exception
     */
    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $block = Block::findOrFail(
            $this->route('block')
        );

        if ($block->getAttribute('game_id') !== $game->getAttribute('id')) {
            return 0;
        }

        $block->getAttribute('blockable')->delete();

        return $this->setDataEndRender([
            'Deleted' => $block->delete()
        ], StatusCode::DELETED);
    }
}
