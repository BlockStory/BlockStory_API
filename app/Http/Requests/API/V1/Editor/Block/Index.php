<?php

namespace App\Http\Requests\API\V1\Editor\Block;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\BlockResource;
use App\Models\Editor\Block\Block;
use App\Models\Editor\Chapter;

class Index extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chapter' => 'required|exists:chapters,id'
        ];
    }

    public function execute()
    {
        $chapter = Chapter::findOrFail(
            $this->get('chapter')
        );

        $blocks = Block::where('chapter_id', $chapter->id)->with('blockable')->get();

        return $this->setDataEndRender([
            'blocks' => BlockResource::collection($blocks)
        ]);
    }
}
