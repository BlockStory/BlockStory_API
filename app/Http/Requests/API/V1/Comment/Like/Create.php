<?php

namespace App\Http\Requests\API\V1\Comment\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;

class Create extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-comment-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $comment = Comment::findOrFail(
            $this->route('comment')
        );

        if ($comment->addLike()) {
            return $this->setDataEndRender([
                'Created' => true
            ], StatusCode::CREATED);
        }

        return $this->setErrorEndRender([
            'Created' => 'Like was not created'
        ], StatusCode::NOT_ALLOWED);
    }
}
