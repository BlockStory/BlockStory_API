<?php

namespace App\Http\Requests\API\V1\Comment\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-comment-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $comment = Comment::findOrFail(
            $this->route('comment')
        );

        if ($comment->removeLike()) {
            return $this->setDataEndRender([
                'Deleted' => true
            ], StatusCode::UPDATED);
        }

        return $this->setErrorEndRender([
            'Deleted' => 'Like was not deleted'
        ], StatusCode::NOT_ALLOWED);
    }
}
