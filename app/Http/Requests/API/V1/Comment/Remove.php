<?php

namespace App\Http\Requests\API\V1\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        \DB::beginTransaction();

        try {
            $comment = Comment::findOrFail(
                $this->route('comment')
            );

            $comment->getAttribute('commentable')->decrement('total_comments');

            \DB::commit();

            return $this->setDataEndRender([
                'Deleted' => $comment->delete()
            ], StatusCode::DELETED);
        } catch (\Exception $exception) {
            \DB::rollBack();

            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
