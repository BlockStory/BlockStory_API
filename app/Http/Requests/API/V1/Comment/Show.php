<?php

namespace App\Http\Requests\API\V1\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Comment\Comment;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'model_type' => 'required|string|in:' . implode(',', array_keys(Comment::MODEL_TYPES)),
            'model_id' => 'required|string'
        ];
    }

    public function execute()
    {
        $model = (Comment::MODEL_TYPES[$this->get('model_type')])::findOrFail(
            $this->get('model_id')
        );

        $model->loadComments();

        return $this->setDataEndRender([
            'comments' => CommentResource::collection($model->getAttribute('comments'))
        ]);
    }
}
