<?php

namespace App\Http\Requests\API\V1\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'model_type' => 'required|string|in:' . implode(',', array_keys(Comment::MODEL_TYPES)),
            'model_id' => 'required|string',
            'parent' => 'sometimes|nullable|exists:comments,id',
            'text' => 'required|string'
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        \DB::beginTransaction();

        try {
            $model = (Comment::MODEL_TYPES[$this->get('model_type')])::findOrFail(
                $this->get('model_id')
            );

            if (!$model->getAttribute('allow_comments')) {
                throw new \Exception('Model not allowed comments');
            }

            $parentComment = null;

            if ($this->get('parent')) {
                $parentComment = Comment::findOrFail($this->get('parent'));

                if ($parentComment->getAttribute('commentable_id') !== $model->getAttribute('id')
                    || $parentComment->getAttribute('commentable_type') !== get_class($model)) {
                    throw new \Exception('Parent comment not in the same commentable');
                }
            }

            $comment = $model->addComment(
                $this->get('text'),
                $parentComment ? $parentComment->getAttribute('id') : null
            );

            \DB::commit();

            return $this->setDataEndRender([
                'comment' => new CommentResource($comment->fresh(['user']))
            ], StatusCode::CREATED);
        } catch (\Exception $exception) {
            \DB::rollBack();

            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
