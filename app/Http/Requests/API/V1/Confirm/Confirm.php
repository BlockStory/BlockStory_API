<?php

namespace App\Http\Requests\API\V1\Confirm;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Trust\Role;
use App\Models\User\Confirmation;
use App\Modules\StatusCode;
use Carbon\Carbon;

class Confirm extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|string|in:' . implode(',', Confirmation::TYPES),
            'code' => 'required|string',
            'hash' => [
                'required',
                function ($attribute, $value, $fail) {
                    $hash = (new Confirmation())->toHash(
                        $this->get('type'),
                        $this->get('code')
                    );

                    if ($hash === $value) {
                        return true;
                    } else {
                        return $fail('Hash is not valid');
                    }
                }
            ]
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws RenderApiException
     */
    public function execute()
    {
        $confirmation = Confirmation::query()
            ->where('type', $this->get('type'))
            ->where('code', $this->get('code'))
            ->firstOrFail();

        if ($confirmation->getAttribute('expired_at')->lt(Carbon::now())) {
            throw new RenderApiException($this->setErrorEndRender([
                'Code was expired'
            ], StatusCode::CONFIRM_CODE_EXPIRED));
        }

        if ($confirmation->getAttribute('accepted_at')) {
            throw new RenderApiException($this->setErrorEndRender([
                'Code was already accepted'
            ], StatusCode::CONFIRM_CODE_ALREADY_ACCEPTED));
        }

        if ($confirmation->getAttribute('type') === Confirmation::TYPE_USER_CREATED) {
            $user = $confirmation->getAttribute('user');

            $user->email_confirmed = true;
            $user->save();

            $user->attachRole('user');
        }

        $confirmation->setAttribute('accepted_at', Carbon::now()->toDateTimeString());
        $confirmation->save();

        return $this->setDataEndRender([
            'Confirm code' => 'OK'
        ], StatusCode::UPDATED);
    }
}
