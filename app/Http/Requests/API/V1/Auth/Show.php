<?php

namespace App\Http\Requests\API\V1\Auth;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\UserResource;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return \Auth::check();
    }

    public function rules()
    {
        return [
        ];
    }

    public function execute()
    {
        $user = \Auth::user();

        $user->roles;

        return $this->setDataEndRender([
            'user' => new UserResource($user)
        ]);
    }
}
