<?php

namespace App\Http\Requests\API\V1\Play;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Play\Play;
use App\Modules\StatusCode;

class Destroy extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        $play = Play::findOrFail(
            $this->route('play')
        );

        return $this->setDataEndRender([
            'Deleted' => $play->delete()
        ], StatusCode::DELETED);
    }
}
