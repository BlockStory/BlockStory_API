<?php

namespace App\Http\Requests\API\V1\Play;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PlayResource;
use App\Models\Game\Game;
use App\Models\Play\Play;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'game' => 'required|string|exists:games,id'
        ];
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        try {
            $game = Game::findOrFail(
                $this->get('game')
            );

            $starterChapter = $game->getAttribute('starterChapter');

            if (!$starterChapter) {
                throw new \Exception('Starter chapter not set');
            }

            $starterBlock = $starterChapter->getAttribute('starterBlock');

            if (!$starterBlock) {
                throw new \Exception('Starter block not set');
            }

            $play = Play::create([
                'user_id' => Auth::id(),
                'game_id' => $game->getAttribute('id'),
                'current_block_id' => $starterBlock->id
            ]);

            return $this->setDataEndRender([
                'play' => new PlayResource($play->fresh())
            ], StatusCode::CREATED);

        } catch (\Exception $exception) {
            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
