<?php

namespace App\Http\Requests\API\V1\Play;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PlayResource;
use App\Models\Play\Play;
use Auth;

class Show extends ApiFormRequest
{
    protected $play;

    public function authorize()
    {
        $this->play = Play::findOrFail(
            $this->route('play')
        );

        if ($this->play->getAttribute('user_id') == Auth::id()) {
            return true;
        }

        return false;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        try {
            $play = Play::findOrFail($this->route('play'));

            $play = $play->goNext();

            return $this->setDataEndRender([
                'play' => new PlayResource($play)
            ]);
        } catch (\Exception $exception) {
            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
