<?php

namespace App\Http\Requests\API\V1\Play;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PlayResource;
use App\Models\Play\Play;
use App\Modules\StatusCode;

class Update extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nextBlock' => 'sometimes|required|exists:blocks,id',
            'title' => 'sometimes|required|string|min:1'
        ];
    }

    public function execute()
    {
        $play = Play::findOrFail(
            $this->route('play')
        );

        if ($this->exists('nextBlock')) {
            try {
                $play = $play->goNext($this->get('nextBlock'));

                return $this->setDataEndRender([
                    'play' => new PlayResource($play)
                ], StatusCode::UPDATED);
            } catch (\Exception $exception) {
                return $this->setErrorEndRender([
                    'Error' => $exception->getMessage()
                ]);
            }
        }

        if ($this->exists('title')) {
            $play->setAttribute('title', $this->get('title'));
        }

        return $this->setDataEndRender([
            'Updated' => $play->save()
        ]);
    }
}
