<?php

namespace App\Http\Requests\API\V1\Tag;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\TagResource;
use App\Models\Tag\Tag;

class Search extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'q' => 'required|string'
        ];
    }

    public function execute()
    {
        $tags = Tag::where('title', 'like', '%' . $this->get('q') . '%')->get();

        return $this->setDataEndRender([
            'tags' => TagResource::collection($tags)
        ]);
    }
}
