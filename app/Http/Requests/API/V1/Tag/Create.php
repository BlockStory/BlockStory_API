<?php

namespace App\Http\Requests\API\V1\Tag;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\TagResource;
use App\Models\Tag\Tag;
use App\Modules\StatusCode;
use Auth;

class Create extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-tag');
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:tags,title'
        ];
    }

    public function execute()
    {
        $tag = Tag::create([
            'user_id' => Auth::user()->id,
            'title' => $this->get('title')
        ]);

        return $this->setDataEndRender([
            'tag' => new TagResource($tag)
        ], StatusCode::CREATED);
    }
}
