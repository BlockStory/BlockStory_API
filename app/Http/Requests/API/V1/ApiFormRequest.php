<?php

namespace App\Http\Requests\API\V1;

use App\Exceptions\RenderApiException;
use App\Modules\Output;
use App\Modules\StatusCode;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

abstract class ApiFormRequest extends FormRequest implements ApiFormRequestInterface
{
    use Output;

    abstract public function execute();

    /**
     * @throws RenderApiException
     */
    protected function failedAuthorization()
    {
        throw new RenderApiException(
            $this->setErrorEndRender([
                'Authorization' => 'User not authorized'
            ], StatusCode::NOT_AUTHORIZED, Response::HTTP_UNAUTHORIZED)
        );
    }

    /**
     * @param Validator $validator
     *
     * @throws RenderApiException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new RenderApiException(
            $this->setErrorValidatorEndRender($validator)
        );
    }
}