<?php

namespace App\Http\Requests\API\V1\File\Upload;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\FileResource;
use App\Models\File\File;
use App\Modules\StatusCode;
use Auth;

class Image extends ApiFormRequest
{
    public function authorize()
    {
        return \Auth::check()
            && Auth::user()->can('upload-image')
            && Auth::user()->limit_files > Auth::user()->files()->count()
            && Auth::user()->limit_files_size > Auth::user()->files()->sum('size') + $this->file('file')->getSize();
    }

    public function rules()
    {
        return [
            'file' => 'required|file|image|mimes:jpg,png,jpeg|max:5000',
        ];
    }

    public function execute()
    {
        $fileInput = $this->file('file');

        $size = getimagesize($fileInput);

        $image = \App\Models\File\Image::create([
            'width' => $size[0],
            'height' => $size[1],
            'bits' => $size['bits']
        ]);

        \Auth::user()->increment('total_images');
        \Auth::user()->increment('uses_files');
        \Auth::user()->increment('uses_files_size', $fileInput->getSize());

        $file = File::uploaded($fileInput, $image->id, get_class($image), File::TYPE_IMAGE);

        $file->fileable;

        return $this->setDataEndRender([
            'file' => new FileResource($file)
        ], StatusCode::CREATED);
    }
}
