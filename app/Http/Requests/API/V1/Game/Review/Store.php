<?php

namespace App\Http\Requests\API\V1\Game\Review;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ReviewResource;
use App\Models\Game\Game;
use App\Models\Review\Review;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-game-review');
    }

    public function rules()
    {
        return [
            'title' => 'required|string|min:1|max:255'
        ];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $review = new Review([
            'user_id' => Auth::id(),
            'title' => $this->get('title')
        ]);

        if ($review = $game->addReview($review)) {
            return $this->setDataEndRender([
                'review' => new ReviewResource($review)
            ]);
        }

        return $this->setErrorEndRender([
            'Created' => 'Not created'
        ]);
    }
}
