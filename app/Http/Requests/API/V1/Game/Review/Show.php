<?php

namespace App\Http\Requests\API\V1\Game\Review;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\ReviewResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $reviews = $game->reviews()->where('published', true);

        if ($this->exists('with')) {

            $with = $this->get('with');

            if (in_array('user', $with)) {
                $reviews->with('user');
            }
        }

        return ReviewResource::collection($reviews->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
