<?php

namespace App\Http\Requests\API\V1\Game\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Auth;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-game-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        if ($game->removeLike()) {
            return $this->setDataEndRender([
                'Deleted' => true
            ], StatusCode::UPDATED);
        }

        return $this->setErrorEndRender([
            'Deleted' => 'Like was not deleted'
        ], StatusCode::NOT_ALLOWED);
    }
}
