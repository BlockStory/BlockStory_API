<?php

namespace App\Http\Requests\API\V1\Game\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-game-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        if ($game->addLike()) {
            return $this->setDataEndRender([
                'Created' => true
            ], StatusCode::CREATED);
        }

        return $this->setErrorEndRender([
            'Created' => 'Like was not created'
        ], StatusCode::NOT_ALLOWED);
    }
}
