<?php

namespace App\Http\Requests\API\V1\Game;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\GameResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class Index extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $games = Game::query();

        $games
            ->when($this->exists('where'), function (Builder $q) {
                $where = is_array($this->get('where'))
                    ? (array)$this->get('where')
                    : (array)json_decode($this->get('where'), true);

                return $q->when(key_exists('user', $where), function () use ($q, $where) {
                   return $q->where('user_id',$where['user']);
                });
            })
            ->when($this->exists('with') && is_array($this->get('with')), function (Builder $q) {
                $with = (array)$this->get('with');

                return $q->when(in_array('user', $with), function () use ($q) {
                    return $q->with('user');
                })->when(in_array('starter', $with), function () use ($q) {
                    return $q->with('starterChapter');
                });
            })
            ->when(true, function (Builder $q) {
               return $q->with([
                   'plays' => function () use ($q) {
                        $q->where('user_id', '=', Auth::id());
                   }
               ]);
            });

        return GameResource::collection($games->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
