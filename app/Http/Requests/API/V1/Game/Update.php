<?php

namespace App\Http\Requests\API\V1\Game;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Editor\Chapter;
use App\Models\Game\Game;
use App\Models\Tag\Tag;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Update extends ApiFormRequest
{
    public $game;

    public function authorize()
    {
        $this->game = Game::findOrFail(
            $this->route('game')
        );

        return Auth::check()
            && Auth::user()->can('update-game')
            && (Auth::user()->owns($this->game) || Auth::user()->can('edit-game-all'));
    }

    public function rules()
    {
        return [
            'title'     => 'sometimes|string|max:128',
            'intro'     => 'sometimes|nullable|string|max:128',
            'description'      => 'sometimes|nullable|string',
            'cover'     => 'sometimes|nullable|string|url',
            'published' => 'sometimes|required|boolean',
            'allow_comments'  => 'sometimes|required|boolean',
            'tags'      => 'sometimes|nullable|array|max:10',
            'sandbox'   => 'sometimes|required|boolean',
            'voting'    => 'sometimes|required|boolean',
            'starter_chapter' => 'sometimes|nullable|string'
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function execute()
    {
        \DB::beginTransaction();

        try {
            $safeParams = [
                'title', 'intro', 'description', 'cover', 'allow_comments', 'published', 'sandbox'
            ];

            foreach ($safeParams as $param) {
                if ($this->exists($param)) {
                    $this->game->{$param} = $this->get($param);
                }
            }

            if ($this->exists('starter_chapter')) {
                if ($this->get('starter_chapter') == null) {
                    $this->game->starter_chapter_id = null;
                } else {
                    $chapter = Chapter::findOrFail(
                        $this->get('starter_chapter')
                    );

                    if ($chapter->getAttribute('game_id') !== $this->game->id) {
                        throw new \Exception('Chapter not belong to game');
                    }

                    $this->game->starter_chapter_id = $this->get('starter_chapter');
                }
            }

            if ($this->exists('tags')) {
                $this->game->syncTags(
                    $this->get('tags')
                );
            }

            if ($this->game->isDirty('sandbox')) {
                if (!Auth::user()->can('edit-game-sandbox')) {
                    throw new RenderApiException(
                        $this->setErrorEndRender([
                            'Authorization' => 'User not authorized to edit sandbox mode'
                        ], StatusCode::NOT_ALLOWED, Response::HTTP_METHOD_NOT_ALLOWED)
                    );
                }
            }

            if ($this->game->isDirty('published')) {
                if ($this->game->published) {
                    $this->game->user->increment('total_games');
                } else {
                    $this->game->user->decrement('total_games');
                }
            }

            $dirty = $this->game->getDirty();

            $this->game->save();

            \DB::commit();

            return $this->setDataEndRender([
                'Updated' => $dirty,
            ], StatusCode::UPDATED, Response::HTTP_ACCEPTED);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
