<?php

namespace App\Http\Requests\API\V1\Game\Rate;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\RateResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $rates = $game->rates();

        if ($this->exists('with') && in_array('user', $this->get('with'))) {
            $rates->with('user');
        }

        return RateResource::collection($rates->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
