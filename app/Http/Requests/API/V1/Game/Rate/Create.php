<?php

namespace App\Http\Requests\API\V1\Game\Rate;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\RateResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Auth;

class Create extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::user()
            && Auth::user()->can('create-game-rate');
    }

    public function rules()
    {
        return [
            'design'    => 'required|integer|min:0|max:10',
            'plot'      => 'required|integer|min:0|max:10',
            'gameplay'  => 'required|integer|min:0|max:10',
            'creativity'    => 'required|integer|min:0|max:10'
        ];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $rate = $game->addRate(
            $this->only([
                'design', 'plot', 'gameplay', 'creativity'
            ])
        );

        if (!$rate) {
            return $this->setErrorEndRender([
                'Forbidden' => 'User already rated'
            ], StatusCode::NOT_ALLOWED);
        }

        $rate->load('user');

        return $this->setDataEndRender([
            'rate' => new RateResource($rate)
        ], StatusCode::CREATED);
    }
}
