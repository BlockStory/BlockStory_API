<?php

namespace App\Http\Requests\API\V1\Game;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\GameResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Illuminate\Http\Response;
use Auth;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return Response
     * @throws RenderApiException
     */
    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

//        if ($game->sandbox) {
//            if (!Auth::check()) {
//                throw new RenderApiException(
//                    $this->setErrorEndRender([
//                        'Authorization' => 'User can\'t see this game'
//                    ],
//                        StatusCode::FORBIDDEN,
//                        Response::HTTP_FORBIDDEN
//                    )
//                );
//            }
//        }
//
//        if (!$game->published) {
//            if (!Auth::check() || (!Auth::user()->owns($game) && !Auth::user()->can('show-game-all'))) {
//                throw new RenderApiException(
//                    $this->setErrorEndRender([
//                        'Authorization' => 'User can\'t see this game'
//                    ],
//                        StatusCode::FORBIDDEN,
//                        Response::HTTP_FORBIDDEN
//                    )
//                );
//            }
//        }

        if ($this->exists('with')) {
            if (in_array('user', $this->get('with'))) {
                $game->load('user');
            }
            if (in_array('tags', $this->get('with'))) {
                $game->load('tags');
            }
            if (in_array('plays', $this->get('with'))) {
                $game->load([
                    'plays' => function ($q) {
                        $q->where('user_id', '=', Auth::id());
                    }
                ]);
            }
        }

        return $this->setDataEndRender([
            'game' => new GameResource($game)
        ]);
    }
}
