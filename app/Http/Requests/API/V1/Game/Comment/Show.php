<?php

namespace App\Http\Requests\API\V1\Game\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Game\Game;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $game->loadComments();

        return $this->setDataEndRender([
            'comments' => CommentResource::collection($game->getAttribute('comments'))
        ]);
    }
}
