<?php

namespace App\Http\Requests\API\V1\Game\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-game-comment');
    }

    public function rules()
    {
        return [
            'parent' => 'sometimes|required|exists:comments,id',
            'text' => 'required|string|max:255'
        ];
    }

    public function execute()
    {
        $game = Game::findOrFail(
            $this->route('game')
        );

        $comment = $game->addComment($this->get('text'), $this->get('parent'));

        $comment->load('user');

        return $this->setDataEndRender([
            'comment' => new CommentResource($comment)
        ], StatusCode::CREATED);
    }
}
