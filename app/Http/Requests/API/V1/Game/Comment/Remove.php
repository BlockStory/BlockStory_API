<?php

namespace App\Http\Requests\API\V1\Game\Comment;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-game-comment');
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return Response
     * @throws RenderApiException
     */
    public function execute()
    {
        $comment = Comment::findOrFail(
            $this->route('comment')
        );

        if (!Auth::user()->owns($comment)) {
            if (!Auth::user()->can('delete-game-comment-all')) {
                throw new RenderApiException($this->setErrorEndRender([
                    'Forbidden' => 'User can\'t delete comment'
                ], StatusCode::FORBIDDEN, Response::HTTP_FORBIDDEN));
            }
        }

        $game = $comment->commentable->fresh();

        return $this->setDataEndRender([
            'Deleted' => $game->deleteComment($comment)
        ], StatusCode::DELETED);
    }
}
