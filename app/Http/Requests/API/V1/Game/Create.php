<?php

namespace App\Http\Requests\API\V1\Game;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\GameResource;
use App\Models\Game\Game;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Create extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-game');
    }

    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function execute()
    {
        $game = Game::create([
            'user_id' => Auth::id(),
            'title' => $this->get('title')
        ]);

        return $this->setDataEndRender([
            'game' => new GameResource($game)
        ], StatusCode::CREATED);
    }
}
