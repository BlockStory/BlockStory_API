<?php

namespace App\Http\Requests\API\V1\Post;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PostResource;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws RenderApiException
     */
    public function execute()
    {
        $post = Post::findOrFail(
            $this->route('post')
        );

        if ($post->getAttribute('sandbox')) {
            if (!Auth::check()) {
                throw new RenderApiException(
                    $this->setErrorEndRender([
                        'Authorization' => 'User can\'t see this post'
                    ],
                        StatusCode::FORBIDDEN,
                        Response::HTTP_FORBIDDEN
                    )
                );
            }
        }

        if (!$post->getAttribute('published')) {
            if (!Auth::check() || (!Auth::user()->owns($post) && !Auth::user()->can('show-post-all'))) {
                throw new RenderApiException(
                    $this->setErrorEndRender([
                        'Authorization' => 'User can\'t see this post'
                    ],
                        StatusCode::FORBIDDEN,
                        Response::HTTP_FORBIDDEN
                    )
                );
            }
        }

        if ($this->exists('with')) {
            if (in_array('user', $this->get('with'))) {
                $post->load('user');
            }
            if (in_array('tags', $this->get('with'))) {
                $post->load('tags');
            }
        }

        return $this->setDataEndRender([
            'post' => new PostResource($post)
        ]);
    }
}
