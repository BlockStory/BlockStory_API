<?php

namespace App\Http\Requests\API\V1\Post;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Post\Post;
use App\Models\Tag\Tag;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Update extends ApiFormRequest
{
    public $post;

    public function authorize()
    {
        $this->post = Post::findOrFail($this->route('post'));

        return Auth::check()
            && Auth::user()->can('update-post')
            && (Auth::user()->owns($this->post) || Auth::user()->can('update-post-all'));
    }

    public function rules()
    {
        return [
            'title'     => 'sometimes|string|max:128',
            'intro'     => 'sometimes|nullable|string|max:128',
            'text'      => 'sometimes|required|string',
            'cover'     => 'sometimes|nullable|string|url',
            'published' => 'sometimes|required|boolean',
            'allow_comments'  => 'sometimes|required|boolean',
            'tags'      => 'sometimes|nullable|array|max:10',
            'sandbox'   => 'sometimes|required|boolean'
        ];
    }

    /**
     * @throws RenderApiException
     */
    public function execute()
    {
        $safeParams = [
            'title', 'intro', 'text', 'cover', 'allow_comments', 'published', 'sandbox'
        ];

        foreach ($safeParams as $param) {
            if ($this->exists($param)) {
                $this->post->{$param} = $this->get($param);
            }
        }

        if ($this->exists('tags')) {
            $this->post->syncTags(
                $this->get('tags')
            );
        }

        if ($this->post->isDirty('sandbox')) {
            if (!Auth::user()->can('edit-post-sandbox')) {
                throw new RenderApiException(
                    $this->setErrorEndRender([
                        'Authorization' => 'User not authorized to edit sandbox mode'
                    ], StatusCode::NOT_ALLOWED, Response::HTTP_METHOD_NOT_ALLOWED)
                );
            }
        }

        if ($this->post->isDirty('published')) {
            if ($this->post->published) {
                $this->post->user->increment('total_posts');
            } else {
                $this->post->user->decrement('total_posts');
            }
        }

        $dirty = $this->post->getDirty();

        $this->post->save();

        return $this->setDataEndRender([
            'Updated' => $dirty,
        ], StatusCode::UPDATED, Response::HTTP_ACCEPTED);
    }
}
