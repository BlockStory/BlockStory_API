<?php

namespace App\Http\Requests\API\V1\Post\Comment;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Comment\Comment;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-post-comment');
    }

    public function rules()
    {
        return [];
    }

    /**
     * @throws RenderApiException
     */
    public function execute()
    {
        $comment = Comment::findOrFail(
            $this->route('comment')
        );

        if (!Auth::user()->owns($comment)) {
            if (!Auth::user()->can('delete-post-comment-all')) {
                throw new RenderApiException($this->setErrorEndRender([
                    'Forbidden' => 'User can\'t delete comment'
                ], StatusCode::FORBIDDEN, Response::HTTP_FORBIDDEN));
            }
        }

        $post = $comment->commentable->fresh();

        return $this->setDataEndRender([
            'Deleted' => $post->deleteComment($comment)
        ], StatusCode::DELETED);
    }
}
