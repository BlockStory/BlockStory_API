<?php

namespace App\Http\Requests\API\V1\Post\Comment;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\CommentResource;
use App\Models\Post\Post;

class Show extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $post = Post::findOrFail(
            $this->route('post')
        );

        $post = $post->load('comments.user');

        return $this->setDataEndRender([
            'comments' => CommentResource::collection($post->comments)
        ]);
    }
}
