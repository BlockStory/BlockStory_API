<?php

namespace App\Http\Requests\API\V1\Post;

use App\Exceptions\RenderApiException;
use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PostResource;
use App\Models\Post\Post;
use App\Models\Tag\Tag;
use App\Modules\StatusCode;
use Auth;
use Illuminate\Http\Response;

class Create extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-post');
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:128'
        ];
    }

    public function execute()
    {
        $post = Post::create([
            'user_id' => Auth::id(),
            'title' => $this->get('title'),
        ]);

        return $this->setDataEndRender([
            'post' => new PostResource($post)
        ], StatusCode::CREATED);
    }
}
