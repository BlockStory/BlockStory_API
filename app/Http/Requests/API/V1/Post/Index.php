<?php

namespace App\Http\Requests\API\V1\Post;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Http\Resources\API\V1\PostResource;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Auth;

class Index extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $posts = Post::query();

        $posts
            ->when($this->exists('where_user'), function ($q) {
                return $q->where('user_id', $this->get('where_user'));
            })
            ->when($this->exists('with') && in_array('user', $this->get('with')), function ($q) {
                return $q->with('user');
            });

        return PostResource::collection($posts->paginate())
            ->additional([
                'status' => StatusCode::OK
            ]);
    }
}
