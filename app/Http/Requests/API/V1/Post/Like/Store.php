<?php

namespace App\Http\Requests\API\V1\Post\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Auth;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('create-post-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $post = Post::findOrFail(
            $this->route('post')
        );

        if ($post->addLike()) {
            return $this->setDataEndRender([
                'Created' => true
            ], StatusCode::CREATED);
        }

        return $this->setErrorEndRender([
            'Created' => 'Like was not created'
        ], StatusCode::NOT_ALLOWED);
    }
}
