<?php

namespace App\Http\Requests\API\V1\Post\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Auth;

class Remove extends ApiFormRequest
{
    public function authorize()
    {
        return Auth::check()
            && Auth::user()->can('delete-post-like');
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $post = Post::findOrFail(
            $this->route('post')
        );

        if ($post->removeLike()) {
            return $this->setDataEndRender([
                'Deleted' => true
            ], StatusCode::UPDATED);
        }

        return $this->setErrorEndRender([
            'Deleted' => 'Like was not deleted'
        ], StatusCode::NOT_ALLOWED);
    }
}
