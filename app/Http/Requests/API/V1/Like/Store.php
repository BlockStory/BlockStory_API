<?php

namespace App\Http\Requests\API\V1\Like;

use App\Http\Requests\API\V1\ApiFormRequest;
use App\Models\Like\Like;
use App\Modules\StatusCode;

class Store extends ApiFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'model_type' => 'required|in:' . implode(',', array_keys(Like::MODEL_TYPES)),
            'model_id' => 'required|string'
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function execute()
    {
        \DB::beginTransaction();

        try {
            $model = (Like::MODEL_TYPES[$this->get('model_type')])::findOrFail(
                $this->get('model_id')
            );

            if ($model->isLiked()) {
                throw new \Exception('Already liked');
            }

            $like = $model->addLike();

            \DB::commit();

            return $this->setDataEndRender([
                'liked' => true
            ], StatusCode::CREATED);

        } catch (\Exception $exception) {
            \DB::rollBack();
            return $this->setErrorEndRender([
                'Error' => $exception->getMessage()
            ]);
        }
    }
}
