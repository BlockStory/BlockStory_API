<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            'permissions' => $this->when(
                $this->resource->relationLoaded('roles'),
                PermissionResource::collection($this->allPermissions())
            ),
            'slogan' => $this->slogan,
            'description' => clean($this->description),
            'avatar' => $this->avatar,
            'cover' => $this->cover,
            'links' => [
                'vk' => $this->link_vk,
                'fb' => $this->link_fb,
                'email' => $this->link_email,
            ],
            'limits' => [
                'files' => [
                    'total' => $this->limit_files,
                    'size' => $this->limit_files_size
                ],
                'uses' => [
                    'total' => $this->uses_files,
                    'size' => $this->uses_files_size
                ]
            ],
            'totals' => [
                'visible' => [
                    'images' => $this->total_images,
                    'subscribers' => $this->total_subscribers,
                    'subscriptions' => $this->total_subscriptions,
                    'posts' => $this->total_posts,
                    'games' => $this->total_games,
                    'games_coauthor' => $this->total_games_coauthor
                ],
                'hidden' => [
                    'posts' => $this->total_posts_all,
                    'games' => $this->total_games_all,
                    'games_coauthor' => $this->total_games_coauthor_all
                ]
            ]
        ];
    }
}
