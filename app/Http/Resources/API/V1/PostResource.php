<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    public function toArray($request)
    {
        $data =  [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'title' => $this->resource->title,
            'intro' => $this->resource->intro,
            'cover' => $this->resource->cover,
            'text' => clean($this->resource->text),
            'settings' => [
                'sandbox' => $this->resource->sandbox,
                'comments' => $this->resource->allow_comments,
                'published' => $this->resource->published
            ],
            'dates' => [
                'posted' => $this->resource->posted_at ? $this->resource->posted_at->toDateTimeString() : null,
                'updated' => $this->resource->updated_at ? $this->resource->updated_at->toDateTimeString() : null,
                'created' => $this->resource->created_at ? $this->resource->created_at->toDateTimeString() : null
            ],
            'user' => new UserResource($this->whenLoaded('user')),
            'tags' => TagResource::collection($this->whenLoaded('tags')),
            'totals' => [
                'comments' => $this->resource->total_comments,
                'likes' => $this->resource->total_likes,
                'views' => [
                    'all' => $this->resource->total_views,
                    'users' => $this->resource->total_views_users
                ]
            ]
        ];

        if ($this->resource->relationLoaded('likes')) {
            $likes = $this->resource->likes;
            $data['likers'] = UserResource::collection(collect(array_pluck($likes, 'user')));

            if (auth()->check()) {
                $data['isLiked'] = in_array(auth()->id(), array_pluck($likes, 'user_id'));
            }
        }

        return $data;
    }
}
