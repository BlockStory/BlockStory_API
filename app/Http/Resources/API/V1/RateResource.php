<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class RateResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'user' => new UserResource($this->whenLoaded('user')),
            'scores' => [
                'design' => $this->design,
                'plot' => $this->plot,
                'gameplay' => $this->gameplay,
                'creativity' => $this->creativity,
            ],
            'total' => [
                'sum' => $this->total,
                'avg' => number_format($this->avg / 100, 1)
            ]
        ];
    }
}
