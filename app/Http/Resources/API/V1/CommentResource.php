<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request)
    {
        $data =  [
            'id' => $this->resource->id,
            'parent' => $this->resource->parent_id,
            'text' => $this->resource->deleted_at ? 'DELETED' : $this->resource->text,
            'user' => $this->resource->deleted_at ? null : new UserResource($this->whenLoaded('user')),
            'dates' => [
                'updated' => $this->resource->updated_at->toDateTimeString(),
                'created' => $this->resource->created_at->toDateTimeString()
            ],
            'totals' => [
                'likes' => $this->resource->total_likes
            ]
        ];

        if ($this->resource->relationLoaded('likes')) {
            $likes = $this->resource->likes;
            $data['likers'] = UserResource::collection(collect(array_pluck($likes, 'user')));

            if (auth()->check()) {
                $data['isLiked'] = in_array(auth()->id(), array_pluck($likes, 'user_id'));
            }
        }

        return $data;
    }
}
