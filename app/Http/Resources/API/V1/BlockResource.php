<?php

namespace App\Http\Resources\API\V1;
use Illuminate\Http\Resources\Json\JsonResource;

class BlockResource extends JsonResource
{
    public function toArray($request)
    {
        $data =  [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'position' => [
                'top' => $this->resource->position_top,
                'left' => $this->resource->position_left
            ]
        ];

        if ($this->resource->relationLoaded('blockable')) {
            $resource = $this->resource->blockable;
            $instance = get_class($resource);
            $resClass = defined("$instance::RESOURCE")
                ? class_exists($instance::RESOURCE)
                    ? $instance::RESOURCE : null
                : null;

            $data['data'] = [
                'type' => defined("$instance::TYPE") ? $instance::TYPE : null,
                'data' => $resClass ? new $resClass($resource) : null
            ];
        }

        return $data;
    }
}
