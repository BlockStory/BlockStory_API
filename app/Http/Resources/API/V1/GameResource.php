<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    public function toArray($request)
    {
        $data =  [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'title' => $this->resource->title,
            'intro' => $this->resource->intro,
            'cover' => $this->resource->cover,
            'description' => clean($this->resource->description),
            'settings' => [
                'sandbox' => $this->resource->sandbox,
                'comments' => $this->resource->allow_comments,
                'published' => $this->resource->published
            ],
            'dates' => [
                'updated' => $this->resource->updated_at->toDateTimeString(),
                'created' => $this->resource->created_at->toDateTimeString()
            ],
            'user' => new UserResource($this->whenLoaded('user')),
            'tags' => TagResource::collection($this->whenLoaded('tags')),
            'totals' => [
                'comments' => $this->resource->total_comments,
                'likes' => $this->resource->total_likes,
                'views' => [
                    'all' => $this->resource->total_views,
                    'users' => $this->resource->total_views_users
                ]
            ],
            'rate' => [
                'total' => [
                    'rates' => $this->resource->total_rates,
                    'sum' => $this->resource->rate_total
                ],
                'scores' => [
                    'design'        => $this->resource->total_rates
                        ? (double)number_format($this->resource->rate_design / $this->resource->total_rates, 1)
                        : 0,
                    'plot'          => $this->resource->total_rates
                        ? (double)number_format($this->resource->rate_plot / $this->resource->total_rates, 1) : 0,
                    'gameplay'      => $this->resource->total_rates
                        ? (double)number_format($this->resource->rate_gameplay / $this->resource->total_rates, 1) : 0,
                    'creativity'    => $this->resource->total_rates
                        ? (double)number_format($this->resource->rate_creativity / $this->resource->total_rates, 1) : 0
                ]
            ],
            'starterChapter' => new ChapterResource($this->whenLoaded('starterChapter')),
            'plays' => PlayResource::collection($this->whenLoaded('plays'))
        ];

        if ($this->resource->relationLoaded('likes')) {
            $likes = $this->resource->likes;
            $data['likers'] = UserResource::collection(collect(array_pluck($likes, 'user')));

            if (auth()->check()) {
                $data['isLiked'] = in_array(auth()->id(), array_pluck($likes, 'user_id'));
            }
        }

        return $data;
    }
}
