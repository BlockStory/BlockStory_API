<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ChapterResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->resource->title,
            'id' => $this->resource->id,
            'dates' => [
                'updated' => $this->resource->updated_at->toDateTimeString(),
                'created' => $this->resource->created_at->toDateTimeString()
            ],
            'globe' => [
                'starter_block' => $this->resource->starter_block_id
            ],
            'user' => new UserResource($this->whenLoaded('user')),
            'game' => new GameResource($this->whenLoaded('game')),
            'blocks' => BlockResource::collection($this->whenLoaded('blocks'))
        ];
    }
}
