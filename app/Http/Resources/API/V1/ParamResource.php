<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ParamResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'validation' => [
                'type' => $this->resource->type,
                'rule' => $this->resource->regex_rule
            ],
            'default' => $this->resource->default,
            'item' => [
                'is_item' => $this->resource->is_item,
                'title' => $this->resource->item_title,
                'description' => $this->resource->item_description,
                'image' => $this->resource->item_image,
                'always_visible' => $this->resource->item_always_visible,
            ]
        ];
    }
}
