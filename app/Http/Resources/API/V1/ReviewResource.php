<?php

namespace App\Http\Resources\API\V1;

use App\Models\Game\Game;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'text' => clean($this->text),
            'totals' => [
                'comments' => $this->total_comments,
                'likes' => $this->total_likes
            ],
            'dates' => [
                'updated' => $this->updated_at->toDateTimeString(),
                'created' => $this->created_at->toDateTimeString(),
                'published' => $this->published_at ? $this->published_at->toDateTimeString() : null
            ],
            'settings' => [
                'comments' => $this->allow_comments,
                'published' => $this->published
            ],
            'user' => new UserResource($this->whenLoaded('user')),
            'test' => $this->resource
        ];

        if ($this->resource->relationLoaded('reviewable')) {
            $resource = $this->resource->reviewable;
            $instance = get_class($resource);

            if ($instance == Game::class) {
                $data['model'] = [
                    'type' => 'game',
                    'data' => new GameResource($resource)
                ];
            }
        }

        if ($request->exists('with') && in_array('isLiked', $request->get('with'))) {
            $data['isLiked'] = $this->isLiked();
        }

        return $data;
    }
}
