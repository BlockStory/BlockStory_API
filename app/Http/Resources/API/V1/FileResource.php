<?php

namespace App\Http\Resources\API\V1;

use App\Models\File\Image;
use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'options' => [
                'type' => $this->resource->type,
                'size' => $this->resource->size,
                'extension' => [
                    'original' => $this->resource->extension_original,
                    'base' => $this->resource->extension
                ],
            ],
            'path' => $this->resource->path,
            'url' => \Storage::disk('public')->url($this->resource->path),
            'name' => $this->resource->name_original,
            'image' => $this->when(
                $this->resource->relationLoaded('fileable')
                && get_class($this->resource->getRelation('fileable')) === Image::class,
                function () {
                    return new ImageResource($this->resource->fileable);
                }
            )
        ];
    }
}
