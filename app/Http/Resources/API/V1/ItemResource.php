<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'description' => $this->resource->description,
            'image' => $this->resource->image,
            'params' => [
                'default' => (int) $this->resource->default,
                'is_unique' => $this->resource->is_unique,
                'always_visible' => $this->resource->always_visible
            ]
        ];
    }
}
