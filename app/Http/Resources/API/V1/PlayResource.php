<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class PlayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'dates' => [
                'updated' => $this->resource->updated_at->toDateTimeString(),
                'created' => $this->resource->created_at->toDateTimeString()
            ],
            'wait' => [
                'until' => $this->resource->wait_until
            ],
            'game' => new GameResource($this->whenLoaded('game')),
            'data' => [
                'params' => PlayParamResource::collection($this->whenLoaded('params')),
                'block' => new BlockResource($this->whenLoaded('block'))
            ],
            'visual' => [
                'dark' => [
                    'text' => $this->resource->visual_text_color_dark,
                    'background' => [
                        'body' => [
                            'image' => $this->resource->visual_background_body_dark_image,
                            'is_color_gradient' => $this->resource->visual_background_body_dark_is_color_gradient,
                            'color' => $this->resource->visual_background_body_dark_color ? '#' . $this->resource->visual_background_body_dark_color : null,
                            'gradient' => [
                                'first' => $this->resource->visual_background_body_dark_color_gradient_first ? '#' . $this->resource->visual_background_body_dark_color_gradient_first : null,
                                'second' => $this->resource->visual_background_body_dark_color_gradient_second ? '#' . $this->resource->visual_background_body_dark_color_gradient_second : null,
                                'direction' => $this->resource->visual_background_body_dark_color_gradient_direction
                            ]
                        ],
                        'card' => [
                            'image' => $this->resource->visual_background_card_dark_image,
                            'is_color_gradient' => $this->resource->visual_background_card_dark_is_color_gradient,
                            'color' => $this->resource->visual_background_card_dark_color ? '#' . $this->resource->visual_background_card_dark_color : null,
                            'gradient' => [
                                'first' => $this->resource->visual_background_card_dark_color_gradient_first ? '#' . $this->resource->visual_background_card_dark_color_gradient_first : null,
                                'second' => $this->resource->visual_background_card_dark_color_gradient_second ? '#' . $this->resource->visual_background_card_dark_color_gradient_second : null,
                                'direction' => $this->resource->visual_background_card_dark_color_gradient_direction
                            ]
                        ]
                    ]
                ],
                'light' => [
                    'text' => $this->resource->visual_text_color_light,
                    'background' => [
                        'body' => [
                            'image' => $this->resource->visual_background_body_light_image,
                            'is_color_gradient' => $this->resource->visual_background_body_light_is_color_gradient,
                            'color' => $this->resource->visual_background_body_light_color ? '#' . $this->resource->visual_background_body_light_color : null,
                            'gradient' => [
                                'first' => $this->resource->visual_background_body_light_color_gradient_first ? '#' . $this->resource->visual_background_body_light_color_gradient_first : null,
                                'second' => $this->resource->visual_background_body_light_color_gradient_second ? '#' . $this->resource->visual_background_body_light_color_gradient_second : null,
                                'direction' => $this->resource->visual_background_body_light_color_gradient_direction
                            ]
                        ],
                        'card' => [
                            'image' => $this->resource->visual_background_card_light_image,
                            'is_color_gradient' => $this->resource->visual_background_card_light_is_color_gradient,
                            'color' => $this->resource->visual_background_card_light_color ? '#' . $this->resource->visual_background_card_light_color : null,
                            'gradient' => [
                                'first' => $this->resource->visual_background_card_light_color_gradient_first ? '#' . $this->resource->visual_background_card_light_color_gradient_first : null,
                                'second' => $this->resource->visual_background_card_light_color_gradient_second ? '#' . $this->resource->visual_background_card_light_color_gradient_second : null,
                                'direction' => $this->resource->visual_background_card_light_color_gradient_direction
                            ]
                        ]
                    ]
                ]
            ],
            'transitions' => [
                'enter' => $this->resource->transition_enter,
                'leave' => $this->resource->transition_leave
            ]
        ];
    }
}
