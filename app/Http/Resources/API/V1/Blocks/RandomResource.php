<?php

namespace App\Http\Resources\API\V1\Blocks;

use App\Http\Resources\API\V1\Blocks\Random\RandomVariantResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RandomResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'variants' => RandomVariantResource::collection($this->whenLoaded('variants'))
        ];
    }
}
