<?php

namespace App\Http\Resources\API\V1\Blocks\Param;

use App\Http\Resources\API\V1\ParamResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SetParamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'set' => [
                'value_is_param' => $this->resource->value_is_param,
                'param' => $this->resource->value_param_id,
                'value' => $this->resource->value
            ],
            'param' => $this->resource->param_id
        ];
    }
}
