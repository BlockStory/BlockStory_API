<?php

namespace App\Http\Resources\API\V1\Blocks\Param;

use Illuminate\Http\Resources\Json\JsonResource;

class LessParamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'param' => $this->resource->param_id,
            'less' => [
                'value_is_param' => $this->resource->value_is_param,
                'param' => $this->resource->value_param_id,
                'value' => $this->resource->value
            ],
            'next' => [
                'true_block' => $this->resource->next_block_true,
                'false_block' => $this->resource->next_block_false
            ]
        ];
    }
}
