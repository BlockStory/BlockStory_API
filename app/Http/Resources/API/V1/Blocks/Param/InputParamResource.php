<?php

namespace App\Http\Resources\API\V1\Blocks\Param;

use Illuminate\Http\Resources\Json\JsonResource;

class InputParamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'set' => [
                'text' => $this->resource->text,
                'rule' => $this->resource->rule,
            ],
            'param' => $this->resource->param_id
        ];
    }
}
