<?php

namespace App\Http\Resources\API\V1\Blocks;

use Illuminate\Http\Resources\Json\JsonResource;

class BackgroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'body' => [
                'dark' => [
                    'image' => $this->resource->body_dark_image,
                    'is_color_gradient' => $this->resource->body_dark_is_color_gradient,
                    'color' => $this->resource->body_dark_color ? '#' . $this->resource->body_dark_color : null,
                    'gradient' => [
                        'first' => $this->resource->body_dark_color_gradient_first ? '#' . $this->resource->body_dark_color_gradient_first : null,
                        'second' => $this->resource->body_dark_color_gradient_second ? '#' . $this->resource->body_dark_color_gradient_second : null,
                        'direction' => $this->resource->body_dark_color_gradient_direction
                    ]
                ],
                'light' => [
                    'image' => $this->resource->body_light_image,
                    'is_color_gradient' => $this->resource->body_light_is_color_gradient,
                    'color' => $this->resource->body_light_color ? '#' . $this->resource->body_light_color : null,
                    'gradient' => [
                        'first' => $this->resource->body_light_color_gradient_first ? '#' . $this->resource->body_light_color_gradient_first : null,
                        'second' => $this->resource->body_light_color_gradient_second ? '#' . $this->resource->body_light_color_gradient_second : null,
                        'direction' => $this->resource->body_light_color_gradient_direction
                    ]
                ]
            ],
            'card' => [
                'dark' => [
                    'image' => $this->resource->card_dark_image,
                    'is_color_gradient' => $this->resource->card_dark_is_color_gradient,
                    'color' => $this->resource->card_dark_color ? '#' . $this->resource->card_dark_color : null,
                    'gradient' => [
                        'first' => $this->resource->card_dark_color_gradient_first ? '#' . $this->resource->card_dark_color_gradient_first : null,
                        'second' => $this->resource->card_dark_color_gradient_second ? '#' . $this->resource->card_dark_color_gradient_second : null,
                        'direction' => $this->resource->card_dark_color_gradient_direction
                    ]
                ],
                'light' => [
                    'image' => $this->resource->card_light_image,
                    'is_color_gradient' => $this->resource->card_light_is_color_gradient,
                    'color' => $this->resource->card_light_color ? '#' . $this->resource->card_light_color : null,
                    'gradient' => [
                        'first' => $this->resource->card_light_color_gradient_first ? '#' . $this->resource->card_light_color_gradient_first : null,
                        'second' => $this->resource->card_light_color_gradient_second ? '#' . $this->resource->card_light_color_gradient_second : null,
                        'direction' => $this->resource->card_light_color_gradient_direction
                    ]
                ]
            ]
        ];
    }
}
