<?php

namespace App\Http\Resources\API\V1\Blocks\Text;

use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'next_block' => $this->next_block
        ];
    }
}
