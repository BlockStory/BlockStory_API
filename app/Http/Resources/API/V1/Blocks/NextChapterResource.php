<?php

namespace App\Http\Resources\API\V1\Blocks;

use Illuminate\Http\Resources\Json\JsonResource;

class NextChapterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
