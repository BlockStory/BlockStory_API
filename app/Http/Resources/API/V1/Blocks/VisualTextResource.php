<?php

namespace App\Http\Resources\API\V1\Blocks;

use Illuminate\Http\Resources\Json\JsonResource;

class VisualTextResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'color' => [
                'light' => $this->resource->color_light ? '#' . $this->resource->color_light : null,
                'dark' => $this->resource->color_dark ? '#' . $this->resource->color_dark : null
            ]
        ];
    }
}
