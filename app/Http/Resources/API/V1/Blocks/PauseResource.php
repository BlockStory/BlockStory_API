<?php

namespace App\Http\Resources\API\V1\Blocks;

use Illuminate\Http\Resources\Json\JsonResource;

class PauseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'text' => $this->resource->text,
            'pause' => [
                'show_time' => $this->resource->show_time,
                'days' => $this->resource->days,
                'hours' => $this->resource->hours,
                'minutes' => $this->resource->minutes,
                'seconds' => $this->resource->seconds,
            ]
        ];
    }
}
