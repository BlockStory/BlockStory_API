<?php

namespace App\Http\Resources\API\V1\Blocks;

use Illuminate\Http\Resources\Json\JsonResource;

class TransitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'next_block' => $this->resource->next_block,
            'animation' => [
                'enter' => $this->resource->enter,
                'leave' => $this->resource->leave
            ]
        ];
    }
}
