<?php

namespace App\Http\Resources\API\V1\Blocks;

use App\Http\Resources\API\V1\Blocks\Text\AnswerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TextSelectAnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'text' => $this->text,
            'variants' => AnswerResource::collection($this->whenLoaded('answers'))
        ];
    }
}
