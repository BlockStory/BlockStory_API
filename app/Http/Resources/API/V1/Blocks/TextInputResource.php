<?php

namespace App\Http\Resources\API\V1\Blocks;

use App\Http\Resources\API\V1\ParamResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TextInputResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'text' => $this->resource->text,
            'next_block' => $this->resource->next_block,
            'param_id' => $this->resource->param_id,
            'param' => new ParamResource($this->whenLoaded('param'))
        ];
    }
}
