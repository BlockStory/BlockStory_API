<?php

namespace App\Http\Resources\API\V1\Blocks\Random;

use Illuminate\Http\Resources\Json\JsonResource;

class RandomVariantResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'next_block' => $this->next_block
        ];
    }
}
