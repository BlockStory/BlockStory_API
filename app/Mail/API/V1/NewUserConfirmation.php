<?php

namespace App\Mail\API\V1;

use App\Models\User\Confirmation;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserConfirmation extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $confirmation;

    public function __construct(User $user, Confirmation $confirmation)
    {
        $this->user = $user;
        $this->confirmation = $confirmation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('v1.emails.users.confirm.newCreatedMarkdown');
    }
}
