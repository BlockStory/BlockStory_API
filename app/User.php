<?php

namespace App;

use App\Models\File\File;
use App\Models\Game\Game;
use App\Models\Post\Post;
use App\Models\Review\Review;
use App\Modules\IdUUID;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, IdUUID, LaratrustUserTrait, HasApiTokens;

    public $incrementing = false;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_confirmed' => 'boolean'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function games()
    {
        return $this->hasMany(Game::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
