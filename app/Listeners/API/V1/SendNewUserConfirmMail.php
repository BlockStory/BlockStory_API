<?php

namespace App\Listeners\API\V1;

use App\Events\API\V1\NewUserCreated;
use App\Mail\API\V1\NewUserConfirmation;
use App\Models\User\Confirmation;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendNewUserConfirmMail
{
    public function __construct()
    {
        //
    }

    public function handle(NewUserCreated $event)
    {
        $confirmation = Confirmation::create([
            'user_id' => $event->user->id,
            'type' => Confirmation::TYPE_USER_CREATED,
            'expired_at' => Carbon::now()->addHour(1)->toDateTimeString()
        ]);

        \Mail::to($event->user->email)
            ->send(new NewUserConfirmation($event->user, $confirmation));
    }
}
