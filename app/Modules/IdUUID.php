<?php
namespace App\Modules;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait IdUUID
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->{$model->getKeyName()} = Str::orderedUuid();
        });
    }
}