<?php

namespace App\Modules;

use App\Models\Comment\Comment;
use App\User;
use Auth;

trait Commentable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function loadComments()
    {
        $toLoad = ['comments.user', 'comments.likes'];

        return $this->load($toLoad);
    }

    /**
     * @param $text
     * @param null $parentId
     * @param User|null $user
     * @return Comment
     * @throws \Exception
     */
    public function addComment($text, $parentId = null, User $user = null)
    {
        $user = $user ?: Auth::user();

        if ($parentId) {
            $parentComment = Comment::findOrFail($parentId);

            if ($parentComment->getAttribute('commentable_id') !== $this->getAttribute('id')
                || $parentComment->getAttribute('commentable_type') !== get_class($this)) {
                throw new \Exception('Parent comment not in the same commentable');
            }
        }

        $comment = new Comment([
            'user_id' => $user->getAttribute('id'),
            'parent_id' => $parentId,
            'text' => $text
        ]);

        $this->increment('total_comments');
        $this->comments()->save($comment);

        return $comment;
    }

    /**
     * @param Comment $comment
     * @return bool|null
     * @throws \Exception
     */
    public function deleteComment(Comment $comment)
    {
        $this->decrement('total_comments');

        return $comment->delete();
    }
}