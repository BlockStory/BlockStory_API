<?php

namespace App\Modules;


class StatusCode
{
    const UNDEFINED = 0;

    // 1 - 9
    const OK = 1;
    const CREATED = 2;
    const UPDATED = 3;
    const DELETED = 4;

    // 10 - 19
    const BAD_VALIDATION = 10;
    const NOT_AUTHORIZED = 11;
    const NOT_FOUNDED = 12;
    const NOT_ALLOWED = 13;
    const FORBIDDEN = 14;

    // 20 - 29
    const INVALID_USER_CREDENTIALS = 20;

    // 900 - 999
    const CONFIRM_CODE_EXPIRED = 900;
    const CONFIRM_CODE_ALREADY_ACCEPTED = 901;
    const USER_NOT_CONFIRMED_EMAIL = 902;
}