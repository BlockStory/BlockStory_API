<?php

namespace App\Modules;

use App\Models\Tag\Tag;
use Auth;

trait Taggable
{
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function syncTags($tags = [])
    {
        $attachTags = [];

        // TODO: increment usage of a tag ?

        foreach ($tags as $tag) {
            $attachTags[] = (string) Tag::firstOrCreate(
                ['title' => $tag],
                ['user_id' => Auth::id()]
            )->incrementUsage()->id; // ->incrementUsage() ??
        }

        $this->tags()->sync($attachTags);
    }
}