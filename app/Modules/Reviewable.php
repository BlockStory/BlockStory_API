<?php

namespace App\Modules;

use App\Models\Review\Review;
use App\User;
use Auth;

trait Reviewable
{
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    public function addReview(Review $review, User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$this->isReviewed($user)) {

            $user->increment('total_reviews_all');
            $this->increment('total_reviews_all');

            return $this->reviews()->save($review);
        }

        return false;
    }

    public function isReviewed(User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$user) return false;

        $review = $this->reviews()->where([
            ['user_id', $user->id],
            ['reviewable_id', $this->id],
            ['reviewable_type', get_class($this)]
        ])->first();

        return $review ?? false;
    }
}