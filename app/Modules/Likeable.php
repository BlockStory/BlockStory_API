<?php

namespace App\Modules;

use App\Models\Like\Like;
use App\User;
use Auth;

trait Likeable
{

    /**
     * @return mixed
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function addLike(User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$user) return false;

        if (!$this->isLiked($user)) {

            $like = new Like([
                'user_id' => $user->getAttribute('id')
            ]);

            $this->increment('total_likes');
            return $this->likes()->save($like);
        }
        
        return false;
    }

    /**
     * @param User|null $user
     * @return bool|null
     * @throws \Exception
     */
    public function removeLike(User $user = null)
    {
        if ($like = $this->isLiked($user)) {
            $this->decrement('total_likes');
            return $like->delete();
        }

        return false;
    }

    /**
     * @param User|null $user
     * @return Like|bool
     */
    public function isLiked(User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$user) return false;

        $like = $this->likes()->where([
            ['user_id', $user->getAttribute('id')],
            ['likeable_id', $this->getAttribute('id')],
            ['likeable_type', get_class($this)]
        ])->first();

        return $like ?? false;
    }
}