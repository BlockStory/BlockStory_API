<?php

namespace App\Modules;

use App\Models\Rate\Rate;
use App\User;
use Auth;
use DB;

trait Rateable
{
    public function rates()
    {
        return $this->morphMany(Rate::class, 'rateable');
    }

    public function addRate($params = [], User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$user) return false;

        if (!$this->isRated($user)) {

            $rate = new Rate([
                'user_id'   => $user->id,
                'design'    => $design      = array_get($params, 'design', 0),
                'plot'      => $plot        = array_get($params, 'plot', 0),
                'gameplay'  => $gameplay    = array_get($params, 'gameplay', 0),
                'creativity'=> $creativity  = array_get($params, 'creativity', 0),
                'total'     => $total       = $design + $plot + $gameplay + $creativity,
                'avg'       => ($total / 4) * 100
            ]);

            $this->update([
                'total_rates' => DB::raw('total_rates + 1'),
                'rate_total' => DB::raw('rate_total + ' . $total),
                'rate_design' => DB::raw('rate_design + ' . $design),
                'rate_plot' => DB::raw('rate_plot + ' . $plot),
                'rate_gameplay' => DB::raw('rate_gameplay + ' . $gameplay),
                'rate_creativity' => DB::raw('rate_creativity + ' . $creativity)
            ]);

            return $this->rates()->save($rate);
        }

        return false;
    }

    public function isRated(User $user = null)
    {
        $user = $user ?: Auth::user();

        if (!$user) return false;

        $rate = $this->rates()->where([
            ['user_id', $user->id],
            ['rateable_id', $this->id],
            ['rateable_type', get_class($this)]
        ])->first();

        return $rate ?? false;
    }
}