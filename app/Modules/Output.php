<?php

namespace App\Modules;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;

trait Output
{
    private $statusCode = StatusCode::OK;
    private $status = Response::HTTP_OK;
    private $body = [];
    private $errors = [];

    public function setStatusCode($code)
    {
        $this->statusCode = $code;

        return $this;
    }

    public function setData($data = [])
    {
        $this->body = array_merge($this->body, $data);

        return $this;
    }

    public function setError($errors = [])
    {
        $this->errors = array_merge($this->errors, $errors);

        return $this;
    }

    public function setDataEndRender($data, $code = StatusCode::OK, $status = null)
    {
        return $this->setData($data)
            ->setStatusCode($code)
            ->setStatus($status)
            ->render();
    }

    public function setErrorEndRender($error, $code = StatusCode::UNDEFINED, $status = null)
    {
        return $this->setError($error)
            ->setStatusCode($code)
            ->setStatus($status)
            ->render();
    }

    public function setErrorValidatorEndRender(Validator $validator)
    {
        $this->errors = $validator->errors();

        return $this->setStatusCode(StatusCode::BAD_VALIDATION)
                    ->setStatus(Response::HTTP_BAD_REQUEST)
                    ->render();
    }

    public function setStatus($status = null)
    {
        if ($status) {
            $this->status = $status;
        }

        return $this;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $response['status'] = $this->statusCode;

        if (count($this->body)) {
            $response['body'] = $this->body;
        }

        if (count($this->errors)) {
            $response['errors'] = $this->errors;
        }

        return response()->json($response, $this->status);
    }
}