<?php

namespace App\Observers;
use App\Models\Game\Game;
use App\User;

class GameObserve
{
    public function creating(Game $post)
    {
        $post->slug = md5(microtime());
    }

    public function created(Game $game)
    {
        $game = $game->fresh();
        $user = User::findOrFail($game->user_id);

        $user->increment('total_games_all');
    }
}
