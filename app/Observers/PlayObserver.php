<?php

namespace App\Observers;

use App\Models\Play\Play;
use App\Models\Play\Param as PlayParam;

class PlayObserver
{
    public function created(Play $play)
    {
        $game = $play->getAttribute('game');

        $gameParams = $game->getAttribute('params');

        foreach ($gameParams as $param) {
            PlayParam::create([
                'play_id' => $play->getAttribute('id'),
                'param_id' => $param->id,
                'value' => $param->default
            ]);
        }
    }
}
