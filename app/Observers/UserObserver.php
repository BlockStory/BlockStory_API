<?php

namespace App\Observers;

use App\Events\API\V1\NewUserCreated;
use App\Models\Trust\Role;
use App\User;

class UserObserver
{
    public function creating(User $user)
    {
        $user->slug = md5(microtime());
    }

    public function created(User $user)
    {
        event(new NewUserCreated($user));
    }

    public function updated(User $user)
    {
        //
    }

    public function deleted(User $user)
    {
        //
    }
}
