<?php


namespace App\Observers;
use App\Models\Post\Post;
use App\User;

class PostObserver
{
    public function creating(Post $post)
    {
        $post->slug = md5(microtime());
    }

    public function created(Post $post)
    {
        $post = $post->fresh();
        $user = User::findOrFail($post->user_id);

        $user->increment('total_posts_all');
    }
}