<?php

namespace App\Providers;

use App\Models\Game\Game;
use App\Models\Play\Play;
use App\Models\Post\Post;
use App\Models\Tag\Tag;
use App\Observers\GameObserve;
use App\Observers\PlayObserver;
use App\Observers\PostObserver;
use App\Observers\TagObserver;
use App\Observers\UserObserver;
use App\User;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        \DB::listen(function ($query) {
//            \Log::info($query->sql, $query->bindings);
//        });

        User::observe(UserObserver::class);
        Post::observe(PostObserver::class);
        Tag::observe(TagObserver::class);
        Game::observe(GameObserve::class);
        Play::observe(PlayObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }
}
