<?php

use Illuminate\Database\Seeder;

class BaseRolesSeeder extends Seeder
{
    public function run()
    {
        $this->call(UserRoleSeeder::class);
        $this->call(ManagerRoleSeeder::class);
    }
}
