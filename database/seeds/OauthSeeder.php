<?php

use Illuminate\Database\Seeder;

class OauthSeeder extends Seeder
{
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'name' => 'BlockStory Password Grant Client',
            'secret' => 'UcGI4gJG9wJ5mBzaIjtbRGaF59lr3WRtZlmy3tAm',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
            'created_at' => $time = \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => $time
        ]);
    }
}
