<?php

use Illuminate\Database\Seeder;

class FirstUsersSeeder extends Seeder
{
    public function run()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'test@mail.ru',
            'password' => 'secret',
            'email_confirmed' => true
        ]);

        $user->attachRole('user');

        factory(\App\Models\Game\Game::class)->create([
            'id' => '8b6bcf03-5cdf-4a49-af37-8e825f611a9a',
            'user_id' => $user->id
        ]);
    }
}
