<?php

use Illuminate\Database\Seeder;
use App\Models\Trust\Role;
use App\Models\Trust\Permission;

class UserRoleSeeder extends Seeder
{
    public function run()
    {
        $user = Role::create([
            'name' => 'user'
        ]);

        $permissions = [];

        collect([
            'create-game',
            'update-game',
            'upload-image',
            'create-post',
            'update-post',
            'create-tag',
            'create-post-like',
            'delete-post-like',
            'create-post-comment',
            'delete-post-comment',
            'create-comment-like',
            'delete-comment-like',
            'create-game-rate',
            'create-game-comment',
            'delete-game-comment',
            'create-game-like',
            'delete-game-like',
            'create-game-review',
            'update-review',
            'create-review-comment',
            'delete-review-comment',
            'create-review-like',
            'delete-review-like'
        ])->each(function ($name) use (&$permissions) {
            $permissions[] = Permission::create([
                'name' => $name
            ]);
        });

        $user->attachPermissions($permissions);
    }
}
