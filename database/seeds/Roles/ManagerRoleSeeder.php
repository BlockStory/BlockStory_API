<?php

use Illuminate\Database\Seeder;
use App\Models\Trust\Role;
use App\Models\Trust\Permission;

class ManagerRoleSeeder extends Seeder
{
    public function run()
    {
        $manager = Role::create([
            'name' => 'manager'
        ]);

        $permissions = [];

        collect([
            'edit-post-sandbox',
            'show-post-all',
            'show-game-all',
            'delete-post-comment-all',
            'edit-game-all'
        ])->each(function ($name) use (&$permissions) {
            $permissions[] = Permission::create([
                'name' => $name
            ]);
        });


        $manager->attachPermissions($permissions);
    }
}
