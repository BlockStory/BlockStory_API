<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('slug', 32)->unique();
            $table->string('name', 128);
            $table->string('email')->unique();
            $table->boolean('email_confirmed')->default(0);
            $table->string('password');

            $table->string('slogan', 128)->nullable();
            $table->text('description')->nullable();

            $table->string('avatar')->nullable();
            $table->string('cover')->nullable();

            $table->string('link_vk')->nullable();
            $table->string('link_fb')->nullable();
            $table->string('link_email')->nullable();

            $table->unsignedInteger('limit_files')->default(300);
            $table->unsignedBigInteger('limit_files_size')->default(500000000); // 500 Мегабайт (в байтах)

            $table->unsignedInteger('uses_files')->default(0);
            $table->unsignedInteger('uses_files_size')->default(0);

            $table->unsignedInteger('total_images')->default(0);

            $table->unsignedInteger('total_subscribers')->default(0);
            $table->unsignedInteger('total_subscriptions')->default(0);
            $table->unsignedInteger('total_posts')->default(0);
            $table->unsignedInteger('total_games')->default(0);
            $table->unsignedInteger('total_games_coauthor')->default(0);
            $table->unsignedInteger('total_posts_all')->default(0);
            $table->unsignedInteger('total_games_all')->default(0);
            $table->unsignedInteger('total_games_coauthor_all')->default(0);
            $table->unsignedInteger('total_reviews')->default(0);
            $table->unsignedInteger('total_reviews_all')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
