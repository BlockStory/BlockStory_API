<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('user_id');

            $table->string('type');
            $table->string('code', 128);
            $table->string('hash', 128);
            $table->json('data')->nullable();

            $table->timestamp('accepted_at')->nullable();
            $table->timestamp('expired_at');
            $table->timestamps();
        });

        Schema::table('user_confirmations', function (Blueprint $table) {
            $table->unique(['type', 'code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_confirmations');
    }
}
