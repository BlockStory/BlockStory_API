<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockVisualTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_visual_texts', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->char('color_light', 8)->nullable();
            $table->char('color_dark', 8)->nullable();

            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_visual_texts');
    }
}
