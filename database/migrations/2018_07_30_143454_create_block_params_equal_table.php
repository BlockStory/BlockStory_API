<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockParamsEqualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_params_equal', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('param_id')->nullable();

            $table->boolean('value_is_param')->default(1);

            $table->string('value')->nullable();
            $table->uuid('value_param_id')->nullable();

            $table->uuid('next_block_true')->nullable();
            $table->uuid('next_block_false')->nullable();

            $table->timestamps();

            $table->foreign('param_id')->references('id')->on('params')->onDelete('set null');
            $table->foreign('value_param_id')->references('id')->on('params')->onDelete('set null');
            $table->foreign('next_block_true')->references('id')->on('blocks')->onDelete('set null');
            $table->foreign('next_block_false')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_params_equal');
    }
}
