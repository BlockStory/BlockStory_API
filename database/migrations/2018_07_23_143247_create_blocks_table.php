<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->nullable();
            $table->uuid('game_id');
            $table->uuid('chapter_id');

            $table->string('title')->default('New block');

            $table->unsignedInteger('position_top')->default(0);
            $table->unsignedInteger('position_left')->default(0);

            $table->uuid('blockable_id');
            $table->string('blockable_type');

            $table->timestamps();

            $table->index(['blockable_type', 'blockable_id']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
            $table->foreign('chapter_id')->references('id')->on('chapters')->onDelete('cascade');
        });

        Schema::table('chapters', function (Blueprint $table) {
            $table->uuid('starter_block_id')->nullable();

            $table->foreign('starter_block_id')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chapters', function (Blueprint $table) {
            $table->dropForeign(['starter_block_id']);
            $table->dropColumn('starter_block_id');
        });

        Schema::dropIfExists('blocks');
    }
}
