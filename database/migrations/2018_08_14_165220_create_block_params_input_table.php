<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockParamsInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_params_input', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('param_id')->nullable();

            $table->string('text')->nullable();
            $table->string('rule')->nullable();

            $table->uuid('next_block')->nullalbe();

            $table->timestamps();

            $table->foreign('param_id')->references('id')->on('params')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_params_input');
    }
}
