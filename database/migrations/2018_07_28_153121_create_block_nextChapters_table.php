<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockNextChaptersTable extends Migration
{
    public function up()
    {
        Schema::create('block_nextChapters', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('next_chapter')->nullable();

            $table->timestamps();

            $table->foreign('next_chapter')->references('id')->on('chapters')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_nextChapters');
    }
}
