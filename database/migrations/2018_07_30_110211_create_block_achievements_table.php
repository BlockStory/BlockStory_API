<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_achievements', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('text')->nullable();
            $table->uuid('badge_id')->nullable();

            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_achievements');
    }
}
