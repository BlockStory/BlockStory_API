<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockPausesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_pauses', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('text')->nullable();

            $table->boolean('show_time')->default(1);

            $table->unsignedTinyInteger('days')->default(0);
            $table->unsignedTinyInteger('hours')->default(0);
            $table->unsignedTinyInteger('minutes')->default(0);
            $table->unsignedTinyInteger('seconds')->default(0);

            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_pauses');
    }
}
