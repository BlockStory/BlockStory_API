<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');

            $table->string('slug', 32)->unique();

            $table->string('title');

            $table->string('cover')->nullable();

            $table->text('intro')->nullable();
            $table->text('description')->nullable();

            $table->boolean('allow_comments')->default(1);
            $table->boolean('sandbox')->default(1);
            $table->boolean('published')->default(0);

            $table->unsignedInteger('total_comments')->default(0);
            $table->unsignedInteger('total_reviews')->default(0);
            $table->unsignedInteger('total_reviews_all')->default(0);
            $table->unsignedInteger('total_views')->default(0);
            $table->unsignedInteger('total_views_users')->default(0);
            $table->unsignedInteger('total_likes')->default(0);

            $table->unsignedInteger('total_rates')->default(0);
            $table->unsignedInteger('rate_total')->default(0);
            $table->unsignedInteger('rate_design')->default(0);
            $table->unsignedInteger('rate_plot')->default(0);
            $table->unsignedInteger('rate_gameplay')->default(0);
            $table->unsignedInteger('rate_creativity')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
