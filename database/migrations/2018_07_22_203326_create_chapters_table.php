<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('game_id');
            $table->uuid('user_id')->nullable();

            $table->string('title');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
        });

        Schema::table('games', function (Blueprint $table) {
            $table->uuid('starter_chapter_id')->nullable();

            $table->foreign('starter_chapter_id')->references('id')->on('chapters')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropForeign(['starter_chapter_id']);
            $table->dropColumn('starter_chapter_id');
        });

        Schema::dropIfExists('chapters');
    }
}
