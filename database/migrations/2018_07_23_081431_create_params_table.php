<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('params', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('game_id');

            $table->string('name', 16);

            $table->string('type', 32)->default(\App\Models\Game\Param::TYPE_STRING);
            $table->string('regex_rule')->nullable();

            $table->string('default')->nullable();

            $table->boolean('is_item')->default(0);

            $table->string('item_title', 32)->nullable();
            $table->string('item_description')->nullable();
            $table->string('item_image')->nullable();

            $table->boolean('item_always_visible')->default(0);

            $table->timestamps();

            $table->unique(['game_id', 'name']);

            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('params');
    }
}
