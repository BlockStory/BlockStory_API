<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plays', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('game_id');

            $table->string('title')->default('New game');

            $table->uuid('current_block_id')->nullable();
            $table->timestamp('wait_until')->nullable();

            $table->string('transition_enter')->nullable();
            $table->string('transition_leave')->nullable();

            $table->char('visual_text_color_light', 8)->nullable();
            $table->char('visual_text_color_dark', 8)->nullable();

            $table->string('visual_background_body_dark_image')->nullable();
            $table->char('visual_background_body_dark_color', 8)->nullable();
            $table->boolean('visual_background_body_dark_is_color_gradient')->default(0);
            $table->char('visual_background_body_dark_color_gradient_first', 8)->nullable();
            $table->char('visual_background_body_dark_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('visual_background_body_dark_color_gradient_direction')->nullable();

            $table->string('visual_background_body_light_image')->nullable();
            $table->char('visual_background_body_light_color', 8)->nullable();
            $table->boolean('visual_background_body_light_is_color_gradient')->default(0);
            $table->char('visual_background_body_light_color_gradient_first', 8)->nullable();
            $table->char('visual_background_body_light_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('visual_background_body_light_color_gradient_direction')->nullable();

            $table->string('visual_background_card_dark_image')->nullable();
            $table->char('visual_background_card_dark_color', 8)->nullable();
            $table->boolean('visual_background_card_dark_is_color_gradient')->default(0);
            $table->char('visual_background_card_dark_color_gradient_first', 8)->nullable();
            $table->char('visual_background_card_dark_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('visual_background_card_dark_color_gradient_direction')->nullable();

            $table->string('visual_background_card_light_image')->nullable();
            $table->char('visual_background_card_light_color', 8)->nullable();
            $table->boolean('visual_background_card_light_is_color_gradient')->default(0);
            $table->char('visual_background_card_light_color_gradient_first', 8)->nullable();
            $table->char('visual_background_card_light_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('visual_background_card_light_color_gradient_direction')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
            $table->foreign('current_block_id')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plays');
    }
}
