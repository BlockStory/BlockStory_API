<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTextInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_text_inputs', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->text('text')->nullable();
            $table->uuid('param_id')->nullable();

            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('param_id')->references('id')->on('params')->onDelete('set null');
            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_text_inputs');
    }
}
