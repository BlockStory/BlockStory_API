<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockVisualBackgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_visual_backgrounds', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('body_dark_image')->nullable();
            $table->char('body_dark_color', 8)->nullable();
            $table->boolean('body_dark_is_color_gradient')->default(0);
            $table->char('body_dark_color_gradient_first', 8)->nullable();
            $table->char('body_dark_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('body_dark_color_gradient_direction')->nullable();

            $table->string('body_light_image')->nullable();
            $table->char('body_light_color', 8)->nullable();
            $table->boolean('body_light_is_color_gradient')->default(0);
            $table->char('body_light_color_gradient_first', 8)->nullable();
            $table->char('body_light_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('body_light_color_gradient_direction')->nullable();

            $table->string('card_dark_image')->nullable();
            $table->char('card_dark_color', 8)->nullable();
            $table->boolean('card_dark_is_color_gradient')->default(0);
            $table->char('card_dark_color_gradient_first', 8)->nullable();
            $table->char('card_dark_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('card_dark_color_gradient_direction')->nullable();

            $table->string('card_light_image')->nullable();
            $table->char('card_light_color', 8)->nullable();
            $table->boolean('card_light_is_color_gradient')->default(0);
            $table->char('card_light_color_gradient_first', 8)->nullable();
            $table->char('card_light_color_gradient_second', 8)->nullable();
            $table->unsignedSmallInteger('card_light_color_gradient_direction')->nullable();

            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_visual_backgrounds');
    }
}
