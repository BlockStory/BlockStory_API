<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTextselectAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_textselect_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('block_id');

            $table->string('title');
            $table->uuid('next_block')->nullable();

            $table->timestamps();

            $table->foreign('block_id')->references('id')->on('block_textselects')->onDelete('cascade');
            $table->foreign('next_block')->references('id')->on('blocks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_textselect_answers');
    }
}
