<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');

            $table->uuid('rateable_id');
            $table->string('rateable_type');

            $table->unsignedTinyInteger('design')->default(0);
            $table->unsignedTinyInteger('plot')->default(0);
            $table->unsignedTinyInteger('gameplay')->default(0);
            $table->unsignedTinyInteger('creativity')->default(0);

            $table->unsignedTinyInteger('total')->default(0);
            $table->unsignedSmallInteger('avg')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
