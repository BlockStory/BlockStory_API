<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('play_params', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('play_id');
            $table->uuid('param_id');

            $table->string('value')->nullable();

            $table->timestamps();

            $table->foreign('play_id')->references('id')->on('plays')->onDelete('cascade');
            $table->foreign('param_id')->references('id')->on('params')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('play_params');
    }
}
