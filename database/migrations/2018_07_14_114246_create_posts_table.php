<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');

            $table->string('slug', 32)->unique();

            $table->boolean('sandbox')->default(1);
            $table->boolean('published')->default(0);

            $table->string('title', 128);
            $table->string('intro', 256)->nullable();
            $table->string('cover')->nullable();

            $table->boolean('allow_comments')->default(1);

            $table->text('text')->nullable();

            $table->unsignedInteger('total_comments')->default(0);
            $table->unsignedInteger('total_views')->default(0);
            $table->unsignedInteger('total_views_users')->default(0);
            $table->unsignedInteger('total_likes')->default(0);

            $table->dateTime('posted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
