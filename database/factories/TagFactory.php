<?php

use Faker\Generator as Faker;
use App\Models\Tag\Tag;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'title' => $faker->unique()->word
    ];
});
