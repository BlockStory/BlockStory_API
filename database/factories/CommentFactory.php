<?php

use Faker\Generator as Faker;
use App\Models\Comment\Comment;

$factory->define(Comment::class, function (Faker $faker) {
    $post = factory(\App\Models\Post\Post::class)->create();

    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'text' => $faker->text,
        'commentable_id' => $post->id,
        'commentable_type' => get_class($post)
    ];
});
