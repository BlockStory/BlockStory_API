<?php

use Faker\Generator as Faker;
use App\Models\Post\Post;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'sandbox' => true,
        'published' => false,
        'title' => $faker->sentence,
        'intro' => $faker->sentences(3, true),
        'cover' => null,
        'allow_comments' => true,
        'text' => $faker->text
    ];
});
