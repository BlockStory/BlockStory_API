@component('vendor.mail.markdown.message')
Добро пожаловать, {{ $user->name }}! <br/>
Чтобы закончить регистрацию, пройди по ссылке ниже:

@component('mail::button', [ 'url' => $confirmation->url() ])
    Продолжить регистрацию
@endcomponent

С уважением,<br/>
{{ config('app.name') }}

@endcomponent