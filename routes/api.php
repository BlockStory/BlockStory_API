<?php

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1' ], function () {

    Route::post('/oauth/token', 'OauthController@issueUserToken')->name('oauth.token');

    Route::get('auth', 'AuthController@getAuthUser')->name('oauth.user');

    Route::get('confirm', 'ConfirmController@confirm')->name('toConfirm');

    Route::group(['prefix' => 'users'], function () {
        Route::post('', 'UserController@store')->name('user.create');
        Route::get('{user}', 'UserController@show')->name('user.show');
        Route::patch('{user}', 'UserController@update')->name('user.update');
//        Route::get('{user}/posts', 'UserController@showPosts')->name('user.show.posts');
//        Route::get('{user}/games', 'UserController@showGames')->name('user.show.games');
//        Route::get('{user}/reviews', 'UserController@showReviews')->name('user.show.reviews');
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::post('', 'PostController@store')->name('post.create');
        Route::get('', 'PostController@index')->name('posts.show');
        Route::get('{post}', 'PostController@show')->name('post.show');
        Route::patch('{post}', 'PostController@update')->name('post.update');
    });

    Route::group(['prefix' => 'games'], function () {
        Route::post('', 'GameController@store')->name('game.create');
        Route::get('', 'GameController@index')->name('game.show');
        Route::get('{game}', 'GameController@show')->name('game.show');
        Route::patch('{game}', 'GameController@update')->name('game.update');
        Route::get('{game}/rates', 'GameController@getRates')->name('game.show.rates');
        Route::post('{game}/rates', 'GameController@storeRate')->name('game.create.rate');
        Route::get('{game}/reviews', 'GameController@getReviews')->name('game.show.reviews');
        Route::post('{game}/reviews', 'GameController@storeReview')->name('game.create.review');
    });

    Route::group(['prefix' => 'plays'], function () {
        Route::get('', 'PlayController@index');
        Route::post('', 'PlayController@store');
        Route::get('{play}', 'PlayController@show');
        Route::patch('{play}', 'PlayController@update');
        Route::delete('{play}', 'PlayController@destroy');
    });

    Route::group(['prefix' => 'reviews'], function () {
        Route::get('{review}', 'ReviewController@show')->name('review.show');
        Route::patch('{review}', 'ReviewController@update')->name('game.update.review');
    });

    Route::group(['prefix' => 'comments'], function () {
        Route::get('', 'CommentController@show')->name('comments.show');
        Route::post('', 'CommentController@store')->name('comment.create');
        Route::delete('{comment}', 'CommentController@delete')->name('comment.remove');
    });

    Route::group(['prefix' => 'likes'], function () {
       Route::post('', 'LikeController@store');
       Route::delete('', 'LikeController@destroy');
    });

    Route::group(['prefix' => 'tags'], function () {
       Route::post('', 'TagController@store')->name('tag.create');
       Route::get('', 'TagController@search')->name('tag.search');
    });

    Route::post('files/upload/image', 'FileController@uploadImage')->name('upload.image');

    Route::group(['prefix' => 'editor', 'namespace' => 'Editor'], function () {

        Route::group(['prefix' => 'game/{game}', 'middleware' => ['canEditGame']], function () {

            Route::get('', 'EditorController@show');

            Route::group(['prefix' => 'chapters'], function () {
                Route::post('', 'ChapterController@store');
                Route::get('{chapter}', 'ChapterController@show');
                Route::patch('{chapter}', 'ChapterController@update');
            });

            Route::group(['prefix' => 'blocks'], function () {
                Route::post('', 'BlockController@store');
                Route::get('', 'BlockController@index');
                Route::patch('{block}', 'BlockController@update');
                Route::delete('{block}', 'BlockController@delete');
            });

            Route::group(['prefix' => 'params'], function () {
                Route::post('', 'ParamController@store');
                Route::get('', 'ParamController@index');
                Route::patch('{param}', 'ParamController@update');
                Route::delete('{param}', 'ParamController@delete');
            });

            Route::group(['prefix' => 'items'], function () {
                Route::post('', 'ItemController@store');
                Route::get('{item}', 'ItemController@show');
                Route::patch('{item}', 'ItemController@update');
                Route::get('', 'ItemController@index');
                Route::delete('{item}', 'ItemController@delete');
            });
        });

    });
});