<?php

use App\Models\Editor\Block\Action\{NextChapter, Checkpoint, Pause};
use App\Models\Editor\Block\Text\{TextInput, TextSelectAnswer};
use App\Models\Editor\Block\Param\{DecrementParam, EqualParam, IncrementParam, SetParam, GreaterParam, LessParam, MultiplyParam, DivideParam, InputParam};
use App\Models\Editor\Block\Money\{Advertisement, Donate};
use App\Models\Editor\Block\Other\{Achievement, Random, Wall};
use App\Models\Editor\Block\Visual\{Transition, Background, VisualText};

return [
    'types' => [
        // ACTIONS
        NextChapter::TYPE       => NextChapter::class,
        Checkpoint::TYPE        => Checkpoint::class,
        Pause::TYPE             => Pause::class,
        // !ACTIONS

        // TEXT
        TextSelectAnswer::TYPE  => TextSelectAnswer::class,
        TextInput::TYPE         => TextInput::class,
        // !TEXT

        // PARAMS
        SetParam::TYPE          => SetParam::class,
        InputParam::TYPE        => InputParam::class,
        EqualParam::TYPE        => EqualParam::class,
        IncrementParam::TYPE    => IncrementParam::class,
        DecrementParam::TYPE    => DecrementParam::class,
        GreaterParam::TYPE      => GreaterParam::class,
        LessParam::TYPE         => LessParam::class,
        MultiplyParam::TYPE     => MultiplyParam::class,
        DivideParam::TYPE       => DivideParam::class,
        // !PARAMS!

        // OTHER
        Wall::TYPE              => Wall::class,
        Random::TYPE            => Random::class,
        Achievement::TYPE       => Achievement::class,
        // !OTHER

        // MONEY
        Advertisement::TYPE     => Advertisement::class,
        Donate::TYPE            => Donate::class,
        // !MONEY

        // VISUAL
        Transition::TYPE        => Transition::class,
        Background::TYPE        => Background::class,
        VisualText::TYPE        => VisualText::class,
        // !VISUAL
    ],
    'data' => [
        'visual' => [
            Transition::TYPE => [
                'transitions' => [
                    'bounce', 'flash', 'pulse', 'rubberBand', 'shake',
                    'headShake', 'swing', 'tada', 'wobble', 'jello', 'heartBeat',
                    'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp',
                    'bounceOut', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp',
                    'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig',
                    'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig',
                    'flip', 'flipInX', 'flipInY', 'flipOutX', 'flipOutY',
                    'lightSpeedIn', 'lightSpeedOut',
                    'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight',
                    'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight',
                    'hinge', 'jackInTheBox',
                    'rollIn', 'rollOut',
                    'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp',
                    'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp',
                    'slideInDown', 'slideInLeft', 'slideInRight', 'slideInUp',
                    'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp'
                ]
            ]
        ]
    ]
];
