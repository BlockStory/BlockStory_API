<?php

namespace Tests\Unit;

use App\Events\API\V1\NewUserCreated;
use App\Mail\API\V1\NewUserConfirmation;
use App\Models\User\Confirmation;
use App\Modules\StatusCode;
use App\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRegistrationTest extends TestCase
{
    public function test_person_can_create_user()
    {
        $email = $this->faker->email;

        $this->postJson(
            route('user.create'),
            [
                'email' => $email,
                'password' => $this->faker->password,
                'name' => $this->faker->name
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);
    }

    public function test_created_user_stored_in_db()
    {
        $email = $this->faker->email;

        $this->postJson(
            route('user.create'),
            [
                'email' => $email,
                'password' => $this->faker->password,
                'name' => $this->faker->name
            ]
        );

        $this->assertDatabaseHas(
            (new User())->getTable(),
            [
                'email' => $email
            ]
        );
    }

    public function test_person_cant_create_user_with_bad_credentials()
    {
        $this->postJson(
            route('user.create'),
            [
                'email' => 'not an email',
                'password' => 'bp', // bad password
                'name' => '' // bad name
            ]
        )->assertJson([
            'status' => StatusCode::BAD_VALIDATION
        ])->assertJsonValidationErrors([
            'email', 'password', 'name'
        ]);
    }

    public function test_created_user_generate_event()
    {
        Event::fake([NewUserCreated::class]);

        $user = $this->create(User::class);

        Event::assertDispatched(NewUserCreated::class, function (NewUserCreated $event) use ($user) {
            return $event->user->id === $user->id;
        });
    }

    public function test_created_user_send_confirmation_mail()
    {
        Mail::fake();

        $user = $this->create(User::class);

        event(new NewUserCreated($user));

        Mail::assertQueued(NewUserConfirmation::class, function (NewUserConfirmation $mail) use ($user) {
            return $mail->user->id === $user->id;
        });
    }

    public function test_user_can_confirm_his_email()
    {
        $user = $this->create(User::class);

        $this->assertDatabaseHas('user_confirmations', [
            'user_id' => $user->id,
            'type' => Confirmation::TYPE_USER_CREATED
        ]);

        $confirmCode = Confirmation::where('user_id', $user->id)
            ->where('type', Confirmation::TYPE_USER_CREATED)
            ->first();

        $this->getJson(
            route('toConfirm', [
                'type' => $confirmCode->type,
                'code' => $confirmCode->code,
                'hash' => $confirmCode->hash
            ])
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $this->assertTrue($user->hasRole('user'));
    }

    public function test_user_cant_confirm_bad_code()
    {
        $this->create(User::class);

        $this->getJson(
            route('toConfirm')
        )->assertJsonValidationErrors([
            'code', 'type', 'hash'
        ]);
    }

    public function test_user_cant_confirm_twice()
    {
        $user = $this->create(User::class);

        $confirmCode = Confirmation::where('user_id', $user->id)
            ->where('type', Confirmation::TYPE_USER_CREATED)
            ->first();

        $this->getJson(
            route('toConfirm', [
                'type' => $confirmCode->type,
                'code' => $confirmCode->code,
                'hash' => $confirmCode->hash
            ])
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $this->getJson(
            route('toConfirm', [
                'type' => $confirmCode->type,
                'code' => $confirmCode->code,
                'hash' => $confirmCode->hash
            ])
        )->assertJson([
            'status' => StatusCode::CONFIRM_CODE_ALREADY_ACCEPTED
        ]);
    }

    public function test_user_cant_enter_without_confirmation()
    {
        $this->create(User::class, [
            'email' => $email = 'test@mail.ru',
            'password' => $password = 'secret'
        ]);

        $this->postJson(
            route('oauth.token'),
            [
                'username' => $email,
                'password' => $password,
                'grant_type' => 'password'
            ]
        )->assertJson([
            'status' => StatusCode::USER_NOT_CONFIRMED_EMAIL
        ]);
    }

    public function test_user_can_enter_with_confirmation()
    {
        $oauth = \DB::table('oauth_clients')->first();

        $user = $this->create(User::class, [
            'email' => $email = 'test@mail.ru',
            'password' => $password = 'secret'
        ]);

        $confirmCode = Confirmation::where('user_id', $user->id)
            ->where('type', Confirmation::TYPE_USER_CREATED)
            ->first();

        $this->getJson(
            route('toConfirm', [
                'type' => $confirmCode->type,
                'code' => $confirmCode->code,
                'hash' => $confirmCode->hash
            ])
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $this->postJson(
            route('oauth.token'),
            [
                'client_id' => $oauth->id,
                'client_secret' => $oauth->secret,
                'username' => $email,
                'password' => $password,
                'grant_type' => 'password'
            ]
        )->assertJson([
            'token_type' => 'Bearer'
        ]);
    }

    public function test_user_can_log_in_via_api()
    {
        $user = $this->signIn();

        $this->getJson(
            route('oauth.user', [
                'user' => $user->id
            ])
        )->assertJson([
            'status' => StatusCode::OK,
            'body' => [
                'user' => [
                    'id' => $user->id
                ]
            ]
        ]);
    }
}
