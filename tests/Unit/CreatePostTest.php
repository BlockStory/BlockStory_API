<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Post\Post;
use App\Modules\StatusCode;

class CreatePostTest extends TestCase
{
    public function test_unauthenticated_user_cant_create_post()
    {
        $this->postJson(
            route('post.create'),
            [
                'title' => $title = $this->faker->sentence,
                'text' => $text = $this->faker->text
            ]
        )->assertJson([
            'status' => StatusCode::NOT_AUTHORIZED
        ]);
    }

    public function test_user_can_create_post()
    {
        $user = $this->signIn();

        $this->postJson(
            route('post.create'),
            [
                'title' => $title = $this->faker->sentence,
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->assertDatabaseHas('posts', [
            'user_id' => $user->id,
            'title' => $title,
        ]);
    }

    public function test_user_total_posts_all_increments_when_new_post()
    {
        $user = $this->signIn();

        $this->postJson(
            route('post.create'),
            [
                'title' => $title = $this->faker->sentence,
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $user = $user->fresh();

        $this->assertEquals(0, $user->total_posts);
        $this->assertEquals(1, $user->total_posts_all);
    }
}
