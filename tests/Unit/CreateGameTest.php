<?php

namespace Tests\Unit;

use App\Modules\StatusCode;
use Tests\TestCase;

class CreateGameTest extends TestCase
{
    public function test_unauthorized_user_cant_create_game()
    {
        $this->postJson(
            route('game.create',[
                'title' => $this->faker->sentence
            ])
        )->assertJson([
            'status' => StatusCode::NOT_AUTHORIZED
        ]);
    }

    public function test_user_can_create_game()
    {
        $user = $this->signIn();

        $this->postJson(
            route('game.create',[
                'title' => $title = $this->faker->sentence
            ])
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->assertDatabaseHas('games', [
            'user_id' => $user->id,
            'title' => $title,
        ]);
    }

    public function test_user_total_games_all_increments_when_new_game()
    {
        $user = $this->signIn();

        $this->postJson(
            route('game.create'),
            [
                'title' => $this->faker->sentence
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $user = $user->fresh();

        $this->assertEquals(0, $user->total_games);
        $this->assertEquals(1, $user->total_games_all);
    }
}
