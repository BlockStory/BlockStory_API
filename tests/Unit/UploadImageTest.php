<?php

namespace Tests\Unit;

use App\Models\File\File;
use App\Modules\StatusCode;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UploadImageTest extends TestCase
{
    public function test_user_can_upload_image()
    {
        $file = 'image.jpg';

        $user = $this->signIn();

        Storage::fake(File::DISK);

        $image = UploadedFile::fake()->image($file, 10, 15);

        $response = $this->postJson(
            route('upload.image'),
            [
                'file' => $image
            ]
        )->assertJson([
            'status' => StatusCode::CREATED,
            'body' => [
                'file' => [
                    'name' => $image->name
                ]
            ]
        ]);

        Storage::disk(File::DISK)->assertExists(
            $response->json()['body']['file']['path']
        );

        $user = $user->fresh();

        $this->assertEquals(1, $user->total_images);
        $this->assertEquals(1, $user->uses_files);
        $this->assertEquals($image->getSize(), $user->uses_files_size);
    }
}
