<?php

namespace Tests;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase, WithFaker;

    public function setUp()
    {
        parent::setUp();

        \Artisan::call('db:seed', ['--class' => 'BaseRolesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OauthSeeder']);
    }

    /**
     * @param $class
     * @param array $attributes
     * @param null $count
     * @param array $states
     * @return Model|Collection
     */
    protected function create($class, $attributes = [], $count = null, $states = [])
    {
        return factory($class, $count)->states($states)->create($attributes);
    }

    /**
     * @param $class
     * @param array $attributes
     * @param null $count
     * @param array $states
     * @return Model|Collection
     */
    protected function make($class, $attributes = [], $count = null, $states = [])
    {
        return factory($class, $count)->states($states)->make($attributes);
    }

    /**
     * @param User|null $user
     * @param array $roles
     * @param array $permissions
     * @param bool $usePassport
     * @return User
     */
    protected function signIn(User $user = null, $roles = ['user'], $permissions = [], $usePassport = true)
    {
        if (!$user) {
            $user = $this->create(User::class);
        }

        $user->attachRoles($roles);
        $user->attachPermissions($permissions);

        $user = $user->fresh();

        if ($usePassport) {
            Passport::actingAs($user);
        } else {
            $this->actingAs($user);
        }

        return $user;
    }
}
