<?php

namespace Tests\Feature;

use App\Models\Game\Game;
use App\Modules\StatusCode;
use Tests\TestCase;

class GameTest extends TestCase
{
    public function test_user_can_see_his_created_game()
    {
        $user = $this->signIn();

        $game = $this->create(Game::class, [
            'user_id' => $user->id
        ]);

        $this->getJson(
            route('game.show', [
                'game' => $game->id
            ])
        )->assertJson([
            'status' => StatusCode::OK
        ]);
    }

    public function test_another_user_cant_see_drafted_game()
    {
        $this->signIn();

        $game = $this->create(Game::class);

        $this->getJson(
            route('game.show', [
                'game' => $game->id
            ])
        )->assertJson([
            'status' => StatusCode::FORBIDDEN
        ]);
    }
}
