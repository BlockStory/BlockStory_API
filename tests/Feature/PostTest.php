<?php

namespace Tests\Feature;

use App\Models\Post\Post;
use App\Modules\StatusCode;
use Illuminate\Http\Response;
use Tests\TestCase;

class PostTest extends TestCase
{
    public function test_user_can_see_his_post()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class, [
            'user_id' => $user->id
        ]);

        $this->getJson(
            route('post.show', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::OK
        ]);
    }

    public function test_another_user_can_not_see_others_drafted_posts()
    {
        $this->signIn();

        $post = $this->create(Post::class);

        $this->getJson(
            route('post.show', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::FORBIDDEN
        ])->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauthenticated_user_cant_see_sandbox_posts()
    {
        $post = $this->create(Post::class);

        $this->getJson(
            route('post.show', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::FORBIDDEN
        ])->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_manager_can_see_sandbox_and_drafted_posts()
    {
        $this->signIn(null, ['user', 'manager']);

        $post = $this->create(Post::class);

        $this->getJson(
            route('post.show', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::OK
        ]);
    }

    public function test_auth_user_can_see_sandbox_posts()
    {
        $this->signIn();

        $post = $this->create(Post::class, [
            'published' => true
        ]);

        $this->getJson(
            route('post.show', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::OK
        ]);
    }

    public function test_user_can_update_post()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class, [
            'user_id' => $user->id,
        ]);

        $this->patchJson(
            route('post.update', [
                'post' => $post
            ]),
            [
                'title' => 'test'
            ]
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);
    }

    public function test_user_can_like_post()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class);

        $this->postJson(
            route('post.create.like', [
                'post' => $post->getAttribute('id')
            ])
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->assertEquals(1, $post->likes()->count());

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => get_class($post)
        ]);
    }

    public function test_user_cant_create_two_likes()
    {
        $this->signIn();

        $post = $this->create(Post::class);

        $this->postJson(
            route('post.create.like', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->postJson(
            route('post.create.like', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::NOT_ALLOWED
        ]);

        $this->assertEquals(1, $post->likes()->count());
    }

    public function test_user_can_remove_like()
    {
        $this->signIn();

        $post = $this->create(Post::class);

        $this->postJson(
            route('post.create.like', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $post = $post->fresh();

        $this->assertEquals(1, $post->likes()->count());
        $this->assertEquals(1, $post->total_likes);

        $this->deleteJson(
            route('post.remove.like', [
                'post' => $post->id
            ])
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $post = $post->fresh();

        $this->assertEquals(0, $post->likes()->count());
        $this->assertEquals(0, $post->total_likes);
    }

    public function test_post_can_has_tags()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class, [
            'user_id' => $user->id
        ]);

        $this->patchJson(
            route('post.update', [
                'post' => $post->id
            ]),
            [
                'title' => $title = $this->faker->sentence,
                'text' => $text = $this->faker->text,
                'tags' => $tags = $this->faker->words
            ]
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $this->assertDatabaseHas('tags', [
            'title' => $tags[0]
        ]);

        $this->assertEquals(
            count($tags),
            Post::first()->tags()->count()
        );
    }

    public function test_user_total_posts_increments_when_new_post()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class, [
            'user_id' => $user->id
        ]);

        $this->patchJson(
            route('post.update', [
                'post' => $post->id
            ]),
            [
                'published' => true
            ]
        )->assertJson([
            'status' => StatusCode::UPDATED
        ]);

        $user = $user->fresh();

        $this->assertEquals(1, $user->total_posts);
        $this->assertEquals(1, $user->total_posts_all);
    }
}
