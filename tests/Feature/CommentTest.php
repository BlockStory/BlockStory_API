<?php

namespace Tests\Feature;

use App\Models\Comment\Comment;
use App\Models\Post\Post;
use App\Modules\StatusCode;
use Tests\TestCase;

class CommentTest extends TestCase
{
    public function test_user_can_create_comment()
    {
        $this->signIn();

        $post = $this->create(Post::class);

        $this->postJson(
            route('post.create.comment', [
                'post' => $post->getAttribute('id')
            ]),
            [
                'text' => $this->faker->text
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $post = $post->fresh();

        $this->assertEquals(1, $post->getAttribute('total_comments'));
    }

    public function test_user_can_get_comments()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class, [
            'user_id' => $user->getAttribute('id')
        ]);

        $comments = $this->create(Comment::class, [
            'commentable_id' => $post->getAttribute('id'),
            'commentable_type' => get_class($post)
        ], 3);

        $this->getJson(
            route('post.show.comments', [
                'post' => $post->getAttribute('id'),
            ])
        )
        ->assertJson([
            'status' => StatusCode::OK,
            'body' => [
                'comments' => [
                    [
                        'id' => $comments[0]->id
                    ],[
                        'id' => $comments[1]->id
                    ],[
                        'id' => $comments[2]->id
                    ],
                ]
            ]
        ]);
    }

    public function test_user_can_delete_comment()
    {
        $user = $this->signIn();

        $post = $this->create(Post::class);

        $comment = $this->create(Comment::class, [
            'user_id' => $user->id,
            'commentable_id' => $post->getAttribute('id'),
            'commentable_type' => get_class($post)
        ]);

        $this->deleteJson(
            route('post.remove.comment', [
                'post' => $post->getAttribute('id'),
                'comment' => $comment->getAttribute('id')
            ])
        )->assertJson([
            'status' => StatusCode::DELETED
        ]);

        $this->assertDatabaseHas('comments', [
            'id' => $comment->getAttribute('id')
        ]);
    }

    public function test_another_user_cant_delete_comment()
    {
        $this->signIn();

        $post = $this->create(Post::class);

        $comment = $this->create(Comment::class, [
            'commentable_id' => $post->getAttribute('id'),
            'commentable_type' => get_class($post)
        ]);

        $this->deleteJson(
            route('post.remove.comment', [
                'post' => $post->getAttribute('id'),
                'comment' => $comment->getAttribute('id')
            ])
        )->assertJson([
            'status' => StatusCode::FORBIDDEN
        ]);
    }

    public function test_manager_can_delete_another_comment()
    {
        $this->signIn(null, ['user', 'manager']);

        $post = $this->create(Post::class);

        $comment = $this->create(Comment::class, [
            'commentable_id' => $post->getAttribute('id'),
            'commentable_type' => get_class($post)
        ]);

        $this->deleteJson(
            route('post.remove.comment', [
                'post' => $post->getAttribute('id'),
                'comment' => $comment->getAttribute('id')
            ])
        )->assertJson([
            'status' => StatusCode::DELETED
        ]);

        $this->assertDatabaseHas('comments', [
            'id' => $comment->getAttribute('id')
        ]);
    }

//    public function test_user_can_like_comment()
//    {
//        $this->signIn();
//
//        $comment = $this->create(Comment::class);
//
//        $this->postJson(
//            route('comment.create.like', [
//                'comment' => $comment->id
//            ])
//        )->assertJson([
//            'status' => StatusCode::CREATED
//        ]);
//
//        $this->assertEquals(1, $comment->fresh()->total_likes);
//    }
//
//    public function test_user_can_delete_like_from_comment()
//    {
//        $this->signIn();
//
//        $comment = $this->create(Comment::class);
//
//        $this->postJson(
//            route('comment.create.like', [
//                'comment' => $comment->id
//            ])
//        )->assertJson([
//            'status' => StatusCode::CREATED
//        ]);
//
//        $this->assertEquals(1, $comment->fresh()->total_likes);
//
//        $this->deleteJson(
//            route('comment.remove.like', [
//                'comment' => $comment->id
//            ])
//        )->assertJson([
//            'status' => StatusCode::UPDATED
//        ]);
//
//        $this->assertEquals(0, $comment->fresh()->total_likes);
//    }
//
//    public function test_another_user_cant_unlike_comment()
//    {
//        $this->signIn();
//
//        $comment = $this->create(Comment::class);
//
//        $this->postJson(
//            route('comment.create.like', [
//                'comment' => $comment->id
//            ])
//        )->assertJson([
//            'status' => StatusCode::CREATED
//        ]);
//
//        $this->assertEquals(1, $comment->fresh()->total_likes);
//
//        $this->signIn();
//
//        $this->deleteJson(
//            route('comment.remove.like', [
//                'comment' => $comment->id
//            ])
//        )->assertJson([
//            'status' => StatusCode::NOT_ALLOWED
//        ]);
//
//        $this->assertEquals(1, $comment->fresh()->total_likes);
//    }
}
