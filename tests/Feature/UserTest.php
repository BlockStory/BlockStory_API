<?php

namespace Tests\Feature;

use App\Modules\StatusCode;
use App\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_user_can_update_information()
    {
        $user = $this->signIn();

        $this->patchJson(
            route('user.update', [
                'user' => $user->id
            ]),
            [
                'name' => $name = $this->faker->name,
                'slug' => $slug = $this->faker->word,
                'slogan' => $slogan = $this->faker->sentence(3),
                'description' => $description = $this->faker->text,
                'link_vk' => $linkVk = $this->faker->url,
                'link_fb' => $linkFb = $this->faker->url,
                'link_email' => $linkEmail = $this->faker->email,
                'avatar' => $avatar = $this->faker->url,
                'cover' => $cover = $this->faker->url,
            ]
        )->assertJson([
            'status' => StatusCode::UPDATED,
            'body' => [
                'updated' => [
                    'name', 'slug', 'slogan', 'description',
                    'link_vk', 'link_fb', 'link_email',
                    'avatar', 'cover'
                ]
            ]
        ]);

        $this->assertDatabaseHas($user->getTable(), [
            'id' => $user->id,
            'name' => $name,
            'slug' => $slug,
            'slogan' => $slogan,
            'description' => $description,
            'link_vk' => $linkVk,
            'link_fb' => $linkFb,
            'link_email' => $linkEmail,
            'avatar' => $avatar,
            'cover' => $cover
        ]);
    }
}
