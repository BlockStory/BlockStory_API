<?php

namespace Tests\Feature;

use App\Models\Tag\Tag;
use App\Modules\StatusCode;
use App\User;
use Tests\TestCase;

class TagTest extends TestCase
{
    public function test_user_can_create_new_tag()
    {
        $user = $this->signIn(null, ['user']);

        $this->postJson(
            route('tag.create'),
            [
                'title' => $title = $this->faker->word
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->assertDatabaseHas('tags', [
            'user_id' => $user->id,
            'title' => $title
        ]);
    }

    public function test_user_cant_create_the_same_tags()
    {
        $this->signIn(null, ['user']);

        $this->postJson(
            route('tag.create'),
            [
                'title' => $title = $this->faker->word
            ]
        )->assertJson([
            'status' => StatusCode::CREATED
        ]);

        $this->postJson(
            route('tag.create'),
            [
                'title' => $title
            ]
        )->assertJsonValidationErrors([
            'title'
        ]);
    }

    public function test_unauthorised_user_cant_create_tag()
    {
        $this->postJson(
            route('tag.create'),
            [
                'title' => $title = $this->faker->word
            ]
        )->assertJson([
            'status' => StatusCode::NOT_AUTHORIZED
        ]);
    }

    public function test_user_can_search_tags()
    {
        $tags = $this->create(Tag::class, [], 3);

        $this->getJson(
            route('tag.search', [
                'q' => $tags[0]->title
            ])
        )->assertJson([
            'status' => StatusCode::OK,
            'body' => [
                'tags' => [
                    [
                        'title' => $tags[0]->title
                    ]
                ]
            ]
        ]);
    }
}
