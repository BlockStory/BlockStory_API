<?php

namespace Tests\Feature;

use App\Models\Trust\Permission;
use App\Models\Trust\Role;
use App\User;
use Tests\TestCase;

class RoleTest extends TestCase
{
    public function test_role_can_be_created()
    {
        $role = Role::create([
            'name' => $name = 'some_role'
        ]);

        $this->assertDatabaseHas(
            $role->getTable(),
            [
                'name' => $name
            ]
        );
    }

    public function test_permission_can_be_created()
    {
        $permission = Permission::create([
            'name' => $name = 'some_permission'
        ]);

        $this->assertDatabaseHas(
            $permission->getTable(),
            [
                'name' => $name
            ]
        );
    }

    public function test_user_can_assign_permission()
    {
        $user = $this->create(User::class);
        $permission = $this->faker->word;

        $createPost = Permission::create([
            'name' => $permission
        ]);

        $user->attachPermission($createPost);

        $this->assertTrue($user->can($permission));
        $this->assertFalse($user->can('do-something-bad'));
    }

    public function test_user_can_detach_permission()
    {
        $user = $this->create(User::class);
        $permission = $this->faker->word;

        $createPost = Permission::create([
            'name' => $permission
        ]);

        $user->attachPermission($createPost);

        $this->assertTrue($user->can($permission));

        $user->detachPermission($createPost);

        $this->assertFalse($user->can($permission));
    }

    public function test_user_can_assign_role()
    {
        $user = $this->create(User::class);
        $role = 'admin';

        $admin = Role::create([
            'name' => $role
        ]);

        $user->attachRole($admin);

        $this->assertTrue($user->hasRole($role));
        $this->assertFalse($user->hasRole('bad-person'));
    }

    public function test_user_can_detach_role()
    {
        $user = $this->create(User::class);
        $role = 'admin';

        $admin = Role::create([
            'name' => $role
        ]);

        $user->attachRole($admin);

        $this->assertTrue($user->hasRole($role));

        $user->detachRole($role);

        $this->assertFalse($user->hasRole($role));
    }
}
